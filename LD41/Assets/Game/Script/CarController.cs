﻿using System.Collections;
using System.Collections.Generic;
using GameJam.Variables;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class AxleInfo
{
  public WheelCollider leftWheel;
  public WheelCollider rightWheel;
  public bool applyMotor;
  public bool applySteering;
  public bool applyBreaking;

  public WheelCollider this[int i]
  {
    get
    {
      return i == 0 ? leftWheel : rightWheel;
    }
  }

}

public class CarController : MonoBehaviour 
{
  
  public int index = 0;

  [Header("Controls")]
  [SerializeField]
  string motorAxis = "Vertical";
  [SerializeField]
  string breakingAxis = "Cancel";
  [SerializeField]
  string steeringAxis = "Horizontal";

  [Header("Steering & Movement")]
  [SerializeField]
  List<AxleInfo> axles = new List<AxleInfo>();

  [SerializeField]
  FloatReference maxMotorTorque;

  [SerializeField]
  FloatReference maxBreakingTorque;

  [SerializeField]
  FloatReference maxSteeringAngle;

  [SerializeField]
  Vector3Reference boostForce;

  [Header("Vehicle Physics Substeps")]
  [SerializeField]
  float criticalSpeed = 5f;

  [SerializeField]
  int stepsBelow = 5;

  [SerializeField]
  int stepsAbove = 1;

  [Header("Suspension Helpers")]
  [Range(0.1f, 20f)]
  public float naturalFrequency = 10;

  [Range(0f, 3f)]
  public float dampingRatio = 0.8f;

  [Range(-1f, 1f)]
  public float forceShift = 0.03f;

  new Rigidbody rigidbody;

  [SerializeField]
  Transform centerOfMass;

  float boostTime = 0;

  AudioSource audioSource = null;

  float pitchVelocity = 0f;
  float volumeVelocity = 0f;

  [SerializeField]
  UnityEvent OnBoosted;

  public bool LockInput
  {
    get; set;
  }

  public float Speed
  {
    get { return rigidbody.velocity.magnitude; }
  }

  private void Awake()
  {
    audioSource = GetComponent<AudioSource>();
    rigidbody = GetComponent<Rigidbody>();
  }

  void Start()
  {
    rigidbody.centerOfMass = centerOfMass.localPosition;
    //This line configures substeps for all wheels (according to unity docs...)
    axles[0].leftWheel.ConfigureVehicleSubsteps(criticalSpeed, stepsBelow, stepsAbove);
  }

  private void OnEnable()
  {
    
  }

  private void OnDisable()
  {
    
  }

  private void Update()
  {
    if ( boostTime > 0 )
    {
      boostTime -= Time.deltaTime;
      rigidbody.AddRelativeForce(boostForce.Value,ForceMode.Acceleration);
    }

    //Update Wheel Suspensions
    for (int i = 0; i < axles.Count; i++)
    {
      var axle = axles[i];
      for (int j = 0; j < 2; j++)
      {
        UpdateSuspension(axle[j]);
      }
    }

    //Audio
    float motorPercent = Input.GetAxis(motorAxis);
    audioSource.volume =  Mathf.SmoothDamp(audioSource.volume,Mathf.Lerp(0.02f, 0.15f, motorPercent),ref volumeVelocity,0.2f);
    audioSource.pitch = Mathf.SmoothDamp(audioSource.pitch, Mathf.Lerp(0.8f, 1.1f, motorPercent), ref pitchVelocity, 0.2f);

    rigidbody.centerOfMass = centerOfMass.localPosition;
  }

  void FixedUpdate()
  {
    float motorInput = 0;
    float breakingInput = 0;
    float steeringInput = 0;

    if ( !LockInput )
    {
      motorInput = Input.GetAxis(motorAxis);
      breakingInput = Input.GetAxis(breakingAxis);
      steeringInput = Input.GetAxis(steeringAxis);
    }

    float motor = maxMotorTorque.Value * motorInput;
    float breaking = maxBreakingTorque.Value * breakingInput;
    float steering = maxSteeringAngle.Value * steeringInput;

    for (int i = 0; i < axles.Count; i++)
    {
      var axle = axles[i];
      for (int j = 0; j < 2; j++)
      {
        if (axle.applyMotor)
        {
          axle[j].motorTorque = motor;
        }
        if (axle.applyBreaking)
        {
          axle[j].brakeTorque = breaking;
        }
        if (axle.applySteering)
        {
          axle[j].steerAngle = steering;
        }
        ApplyLocalPositionToVisuals(axle[j]);
      }
    }
  }

  public void Boost(float duration)
  {
    boostTime += duration;
    OnBoosted.Invoke();
  }

  public void ApplyLocalPositionToVisuals(WheelCollider collider)
  {
    Transform visualWheel = collider.transform.GetChild(0);

    Vector3 position;
    Quaternion rotation;
    collider.GetWorldPose(out position, out rotation);

    visualWheel.transform.position = position;
    visualWheel.transform.rotation = rotation;
  }

  public void UpdateSuspension(WheelCollider wheel)
  {
    JointSpring spring = wheel.suspensionSpring;

    float sqrtWcSprungMass = Mathf.Sqrt(wheel.sprungMass);
    spring.spring = sqrtWcSprungMass * naturalFrequency * sqrtWcSprungMass * naturalFrequency;
    spring.damper = 2f * dampingRatio * Mathf.Sqrt(spring.spring * wheel.sprungMass);

    wheel.suspensionSpring = spring;

    Vector3 wheelRelativeBody = transform.InverseTransformPoint(wheel.transform.position);
    float distance = rigidbody.centerOfMass.y - wheelRelativeBody.y + wheel.radius;

    wheel.forceAppPointDistance = distance - forceShift;

    // Make sure the spring force at maximum droop is exactly zero
    if (spring.targetPosition > 0)
    {
      wheel.suspensionDistance = wheel.sprungMass * Physics.gravity.magnitude / (spring.targetPosition * spring.spring);
    }
  }

}

