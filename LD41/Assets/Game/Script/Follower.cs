﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour
{

  [SerializeField]
  Transform target;

  [SerializeField]
  float positionDamp = 0.5f;

  [SerializeField]
  float rotationDamp = 0.5f;

  Vector3 positionVelocity;
  float rotationalVelcity;

  void Start()
  {

  }

  private void Update()
  {
    
  }

  void LateUpdate()
  {

  }

  private void FixedUpdate()
  {
    DampPosition();
  }

  void DampPosition()
  {
    if (target == null)
    {
      return;
    }

    var targetPt = target.position;
    transform.position = Vector3.SmoothDamp(transform.position, targetPt, ref positionVelocity, positionDamp);

    var targetRot = target.eulerAngles;
    targetRot.x = 0;
    targetRot.z = 0;
    targetRot.y = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRot.y, ref rotationalVelcity, rotationDamp);
    transform.eulerAngles = targetRot;
  }

  private void OnValidate()
  {
    
    if ( target == null )
    {
      return;
    }

    var targetPt = target.position;
    transform.position = targetPt;
    var targetRot = target.eulerAngles;
    targetRot.x = 0;
    targetRot.z = 0;
    transform.eulerAngles = targetRot;
  }

}
