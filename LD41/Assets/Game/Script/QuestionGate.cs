using UnityEngine;
using TMPro;

public class QuestionGate : MonoBehaviour
{
  [SerializeField]
  private TMP_Text text;

  public bool isCorrect;

  public int index = 0;

  public delegate void select();
  public event select answerSelected;

  private void OnTriggerEnter(Collider other)
  {
    var trivia = other.GetComponentInParent<TriviaController>();
    trivia.AnswerQuestion(index);

    /*if (isCorrect)
    {
      Debug.Log("CORRECT");
    }
    else
    {
      Debug.Log("INCORRECT");
    }*/

    answerSelected?.Invoke();
  }

  public void SetAnswer(string answer)
  {
    text.text = answer;
  }

}
