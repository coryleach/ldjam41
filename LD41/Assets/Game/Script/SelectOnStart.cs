﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectOnStart : MonoBehaviour 
{

  Selectable selectable;

  private void OnEnable()
  {
    selectable = GetComponent<Selectable>();
    if (selectable != null)
    {
      selectable.Select();
    }
  }

}
