﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiRoll : MonoBehaviour {

  public WheelCollider leftWheel;
  public WheelCollider rightWheel; 
  public float antiRoll = 5000.0f;

  new Rigidbody rigidbody;

  void Start()
  {
    rigidbody = GetComponentInParent<Rigidbody>();
  }

  void FixedUpdate()
  {
    WheelHit hit;
    float travelL = 1.0f;
    float travelR = 1.0f;

    var groundedL = leftWheel.GetGroundHit(out hit);

    if (groundedL)
    {
      travelL = (-leftWheel.transform.InverseTransformPoint(hit.point).y - leftWheel.radius) / leftWheel.suspensionDistance;
    }

    var groundedR = rightWheel.GetGroundHit(out hit);

    if (groundedR)
    {
      travelR = (-rightWheel.transform.InverseTransformPoint(hit.point).y - rightWheel.radius) / rightWheel.suspensionDistance;
    }

    var antiRollForce = (travelL - travelR) * antiRoll;

    if (groundedL)
    {
      rigidbody.AddForceAtPosition(leftWheel.transform.up * -antiRollForce, leftWheel.transform.position);
    }

    if (groundedR)
    {
      rigidbody.AddForceAtPosition(rightWheel.transform.up * antiRollForce, rightWheel.transform.position);
    }
  }

}
