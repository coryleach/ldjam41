﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyMonitor : MonoBehaviour 
{

  [SerializeField]
  KeyCode key;

  [SerializeField]
  UnityEvent OnKey;
	
	void Update() 
  {
    if ( Input.GetKeyDown(key) )
    {
      OnKey.Invoke();
    }
	}

}
