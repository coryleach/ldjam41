﻿using System.Collections;
using System.Collections.Generic;
using GameJam.Variables;
using TMPro;
using UnityEngine;

public class LapText : MonoBehaviour 
{

  TextMeshProUGUI text;

  [SerializeField]
  int racerIndex = 0;

  [SerializeField]
  IntReference totalLaps;

  void Start()
  {
    text = GetComponent<TextMeshProUGUI>();
  }

  int currentLap = -1;

  void Update()
  {
    
    if (text == null)
    {
      return;
    }

    var racer = RaceController.Current.racers[racerIndex];

    if (racer.finished)
    {
      text.text = "Finished!";
      enabled = false;
    }
    else if ( currentLap != racer.lap )
    {
      currentLap = racer.lap;
      text.text = string.Format("Lap {0}/{1}",racer.lap + 1,totalLaps.Value);
    }

  }

}
