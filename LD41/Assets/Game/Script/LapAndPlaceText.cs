﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LapAndPlaceText : MonoBehaviour
{

  TextMeshProUGUI text;

  [SerializeField]
  int racerIndex = 0;

  void Start()
  {
    text = GetComponent<TextMeshProUGUI>();
  }

  void Update()
  {
    if ( text == null )
    {
      return;
    }

    var racer = RaceController.Current.racers[racerIndex];

    var ordinal = "th";
    if ( racer.place == 1 )
    {
      ordinal = "st";
    }
    else if ( racer.place == 2 )
    {
      ordinal = "nd";
    }
    else if ( racer.place == 3 )
    {
      ordinal = "rd";
    }
     
    if ( racer.finished )
    {
      text.text = string.Format("{0}{1}\nFinished", racer.place, ordinal);
    }
    else
    {
      text.text = string.Format("{0}{1}\nLap {2} ", racer.place, ordinal, racer.lap + 1);
    }

  }

}
