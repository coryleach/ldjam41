using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu]
public class Question : ScriptableObject
{
  public string question;
  public string correct;
  public List<string> wrong;
}
