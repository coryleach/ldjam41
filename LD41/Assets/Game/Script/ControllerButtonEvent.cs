﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerButtonEvent : MonoBehaviour 
{

  public string buttonName;

  public UnityEvent OnButtonDown;

	void Update() 
  {
    if ( Input.GetButtonDown(buttonName) )
    {
      OnButtonDown.Invoke();
    }
	}

}
