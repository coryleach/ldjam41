﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Speedometer : MonoBehaviour 
{

  [SerializeField]
  TextMeshProUGUI text;

  [SerializeField]
  CarController car;

	void Update() 
  {
    text.text = string.Format("Speed {0:0}m/s",car.Speed);
	}

}
