﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyWheelFriction : MonoBehaviour 
{
  
  [System.Serializable]
  public class AppliedWheelFriction
  {
    public float extremumSlip = 0.4f;
    public float extremumValue = 1;
    public float asymptoteSlip = 0.8f;
    public float asymptoteValue = 0.5f;
    public float stiffness = 1;

    public WheelFrictionCurve GetCurve()
    {
      WheelFrictionCurve curve = new WheelFrictionCurve();
      curve.asymptoteSlip = asymptoteSlip;
      curve.asymptoteValue = asymptoteValue;
      curve.extremumSlip = extremumSlip;
      curve.extremumValue = extremumValue;
      curve.stiffness = stiffness;
      return curve;
    }
  }

  [SerializeField]
  WheelFrictionCurve curve;

  public AppliedWheelFriction sidewaysFriction;

  public AppliedWheelFriction forwardFriction;

}
