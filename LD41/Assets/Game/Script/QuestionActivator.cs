using UnityEngine;
using System.Collections.Generic;

public class QuestionActivator : MonoBehaviour
{
  [SerializeField]
  private List<Question> easyQuestions;

  [SerializeField]
  private List<Question> hardQuestions;

  [SerializeField]
  private TriviaSetup setup;

  private void OnTriggerEnter(Collider other)
  {
    var car = other.gameObject.GetComponentInParent<CarController>();
    var trivia = other.gameObject.GetComponentInParent<TriviaController>();

    if ( car == null )
    {
      return;
    }

    if ( trivia == null )
    {
      return;
    }

    var racer = RaceController.Current.racers[car.index];

    var questionList = easyQuestions;

    if ( racer.place == 1 )
    {
      questionList = hardQuestions;
    }

    trivia.SetQuestion(questionList[Random.Range(0, questionList.Count)]);
    //setup.gameObject.SetActive(true);
    //setup.SetQuestion(questions[Random.Range(0, questions.Count)]);
  }

}
