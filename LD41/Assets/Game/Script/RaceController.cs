﻿using System.Collections;
using System.Collections.Generic;
using GameJam.Variables;
using UnityEngine;
using UnityEngine.Events;

public class RaceController : MonoBehaviour
{

  static RaceController current;
  public static RaceController Current
  {
    get { return current; }
  }

  public List<Racer> racers;

  public IntReference totalLaps;
  public FloatReference startDelay;

  [SerializeField]
  UnityEvent OnRaceStart;

  [SerializeField]
  UnityEvent OnSingleRacerFinish;

  [SerializeField]
  UnityEvent OnRaceFinish;

  List<Racer> finishedRacers = new List<Racer>();
  List<Racer> sortedRacers = new List<Racer>();

  List<RaceGate> gates = new List<RaceGate>();

  float delay = 0;
  float time = 0;

  public float Delay
  {
    get { return delay; }
  }

  bool started = false;
  bool finished = false;

  #region Monobehaviour

  private void Awake()
  {
    current = this;
    delay = startDelay.Value;
  }

  void Start()
  {
    for (int i = 0; i < racers.Count; i++)
    {
      racers[i].car.index = i;
      racers[i].car.LockInput = true;
      racers[i].finished = false;
    }
    sortedRacers.AddRange(racers);
  }

  private void Update()
  {
    if (finished)
    {
      return;
    }
    else if (!started)
    {
      delay -= Time.deltaTime;
      if (delay <= 0)
      {
        StartRace();
      }
      return;
    }
    else
    {
      time += Time.deltaTime;
      UpdatePlacing();
    }
  }

  #endregion

  #region Race Start & Finish

  void StartRace()
  {
    started = true;
    for (int i = 0; i < racers.Count; i++)
    {
      racers[i].car.LockInput = false;
    }
    OnRaceStart.Invoke();
  }

  void FinishRace()
  {
    finished = true;
    OnRaceFinish.Invoke();
  }

  void FinishRacer(Racer racer)
  {
    racer.finished = true;
    OnSingleRacerFinish.Invoke();
    sortedRacers.Remove(racer);
    finishedRacers.Add(racer);
    CheckIfEveryoneFinished();
  }

  void CheckIfEveryoneFinished()
  {
    if ( finishedRacers.Count == racers.Count )
    {
      FinishRace();
    }
  }

  #endregion

  public void RegisterCheckpoint(RaceGate gate)
  {
    gates.Add(gate);
    gates.Sort((a, b) => { return a.index.CompareTo(b.index); });
  }

  public void RacerCheckpoint(int playerIndex, int gateIndex)
  {
    var racer = racers[playerIndex];

    if ( racer.finished )
    {
      return;
    }

    //If we hit the first gate after hitting the last gate then complete a lap!
    if (gateIndex == 0 && racer.checkpoint == (gates.Count - 1))
    {
      racer.lap += 1;
      racer.checkpoint = 0;
      if (racer.lap >= totalLaps.Value)
      {
        FinishRacer(racer);
      }
    }

    if (racer.checkpoint == (gateIndex - 1))
    {
      racer.checkpoint = gateIndex;
    }
  }

  public void RacerBacktrack(int playerIndex, int gateIndex)
  {
    var racer = racers[playerIndex];
    if (gateIndex == racer.checkpoint)
    {
      racer.checkpoint -= 1;
    }

    if (racer.checkpoint < 0)
    {
      //Lose a lap if we can but never go negative
      if (racer.lap > 0)
      {
        racer.lap -= 1;
      }
      //We've backed through the last checkpoint
      racer.checkpoint = 0;
    }
  }

  public void UpdatePlacing()
  {
    sortedRacers.Sort((a, b) =>
    {
      int lapCompare = b.lap - a.lap;
      if (lapCompare != 0)
      {
        return lapCompare;
      }

      int checkpointCompare = b.checkpoint - a.checkpoint;
      if (checkpointCompare != 0)
      {
        return checkpointCompare;
      }

      //Compare distance to next checkpoint
      var nextCheckpoint = GetNextCheckpoint(a.checkpoint);
      Debug.LogFormat("Check Checkpoint {0}",nextCheckpoint.name);
      var aDistance = (a.car.transform.position - nextCheckpoint.transform.position).sqrMagnitude;
      var bDistance = (b.car.transform.position - nextCheckpoint.transform.position).sqrMagnitude;
      return aDistance.CompareTo(bDistance);
    });

    for (int i = 0; i < sortedRacers.Count; i++)
    {
      sortedRacers[i].place = finishedRacers.Count + i + 1;
    }
  }

  RaceGate GetNextCheckpoint(int checkpoint)
  {
    if ((checkpoint + 1) >= gates.Count)
    {
      return gates[0];
    }
    else
    {
      return gates[checkpoint + 1];
    }
  }

}

[System.Serializable]
public class Racer
{
  public CarController car;
  public Canvas canvas;
  public int lap = 0;
  public int checkpoint = 0;
  public int place = 0;
  public bool finished = false;
}
