﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RaceCountdown : MonoBehaviour
{

  TextMeshProUGUI text;

  [SerializeField]
  AudioSource audioSource;

  // Use this for initialization
  IEnumerator Start()
  {
    text = GetComponent<TextMeshProUGUI>();

    bool playedAudio = false;

    while (RaceController.Current.Delay > 0.5f)
    {
      int seconds = Mathf.CeilToInt(RaceController.Current.Delay);
      if (RaceController.Current.Delay <= 1)
      {
        text.text = "Start!";
      }
      else if (RaceController.Current.Delay > 3)
      {
        text.text = string.Format("<size=72>{0}</size>", seconds);
      }
      else
      {
        text.text = string.Format("{0}", seconds);
      }

      if (!playedAudio && RaceController.Current.Delay < 3f)
      {
        audioSource.Play();
        playedAudio = true;
      }

      yield return null;
    }

    yield return new WaitForSeconds(0.25f);

    gameObject.SetActive(false);
  }

}
