﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriviaController : MonoBehaviour
{

  int correctAnswers = 0;
  int questionCount = 0;

  Question currentQuestion = null;
  public Question CurrentQuestion
  {
    get { return currentQuestion; }
  }

  public UnityEvent OnCorrect;
  public UnityEvent OnIncorrect;
  public UnityEvent OnNewQuestion;

  List<string> answers = new List<string>();
  public IReadOnlyList<string> Answers
  {
    get { return answers; }
  }

  void Start()
  {

  }

  public void SetQuestion(Question question)
  {
    if ( currentQuestion != null )
    {
      return;
    }

    currentQuestion = question;
    questionCount += 1;

    answers.Clear();

    //Build answer list
    answers.Add(question.correct);
    for (int i = 0; i < question.wrong.Count; i++ )
    {
      answers.Add(question.wrong[i]);
    }

    //Randomize Answer List
    for (int i = 0; i < answers.Count; ++i )
    {
      int swap = Random.Range(i, answers.Count);

      var temp = answers[swap];
      answers[swap] = answers[i];
      answers[i] = temp;
    }

    OnNewQuestion.Invoke();
  }

  public void AnswerQuestion(int index)
  {
    if (currentQuestion == null)
    {
      return;
    }

    var answer = index >= 0 ? answers[index] : string.Empty;

    if ( answer.Equals(currentQuestion.correct) )
    {
      correctAnswers += 1;
      OnCorrect.Invoke();
    }
    else
    {
      OnIncorrect.Invoke();
    }

    currentQuestion = null;
  }

}
