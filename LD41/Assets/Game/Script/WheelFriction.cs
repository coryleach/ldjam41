﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelFriction : MonoBehaviour 
{

  WheelCollider wheelCollider;

	void Start() 
  {
    wheelCollider = GetComponent<WheelCollider>();
	}
	
	void Update() 
  {
    WheelHit hit;
    if ( wheelCollider.GetGroundHit(out hit) )
    {
      //Debug.LogFormat("{0} : Forward Slip {1} Sideways Slip {2} ",name,hit.forwardSlip,hit.sidewaysSlip);
      /*var friction = hit.collider.GetComponent<ApplyWheelFriction>();
      if ( friction != null )
      {
        wheelCollider.sidewaysFriction = friction.sidewaysFriction.GetCurve();
        wheelCollider.forwardFriction = friction.forwardFriction.GetCurve();
      }*/
    }
	}

}
