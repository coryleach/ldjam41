﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceGate : MonoBehaviour 
{

  public int index = 0;

  void Start()
  {
    RaceController.Current.RegisterCheckpoint(this);
  }

  void OnTriggerExit(Collider other)
  {
    var car = other.gameObject.GetComponentInParent<CarController>();
    if ( car == null )
    {
      return;
    }

    var directionToPlayer = other.transform.position - transform.position;
    var myForward = transform.forward;

    var angle = Vector3.Angle(directionToPlayer, myForward);

    if (angle < 90f)
    {
      RaceController.Current.RacerCheckpoint(car.index,index);
    }
    else
    {
      RaceController.Current.RacerBacktrack(car.index, index);
    }
  }

}
