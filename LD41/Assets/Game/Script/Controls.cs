﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour 
{

  public string horizontalAxisName = "Horizontal";
  public string verticalAxisName = "Vertical";
  public string accelerateAxisName = "Submit";
  public string breakingAxisName = "Cancel";

  public float horizontal = 0f;
  public float vertical = 0f;
  public float accelerate = 0f;
  public float breaking = 0f;

  void Update()
  {
    horizontal = Input.GetAxis(horizontalAxisName);
    vertical = Input.GetAxis(verticalAxisName);
    accelerate = Input.GetAxis(accelerateAxisName);
    breaking = Input.GetAxis(breakingAxisName);
  }

}
