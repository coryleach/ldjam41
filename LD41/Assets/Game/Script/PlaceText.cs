﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlaceText : MonoBehaviour 
{

  TextMeshProUGUI text;

  [SerializeField]
  int racerIndex = 0;

  void Start()
  {
    text = GetComponent<TextMeshProUGUI>();
  }

  int currentPlace = -1;

  void Update()
  {
    
    if (text == null)
    {
      return;
    }

    var racer = RaceController.Current.racers[racerIndex];

    if ( racer.place == currentPlace )
    {
      return;
    }

    currentPlace = racer.place;

    if ( racer.place == 0 )
    {
      text.text = string.Empty;
      return;
    }

    var ordinal = "th";
    if (racer.place == 1)
    {
      ordinal = "st";
    }
    else if (racer.place == 2)
    {
      ordinal = "nd";
    }
    else if (racer.place == 3)
    {
      ordinal = "rd";
    }

    if (racer.finished)
    {
      text.text = string.Format("{0}{1}", racer.place, ordinal);
    }
    else
    {
      text.text = string.Format("{0}{1}", racer.place, ordinal);
    }

  }

}
