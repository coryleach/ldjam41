﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TriviaHud : MonoBehaviour
{

  [SerializeField]
  TriviaController trivia;

  [SerializeField]
  TextMeshProUGUI question;

  [SerializeField]
  TextMeshProUGUI[] answers;

  Animator animator;

  // Use this for initialization
  void Start()
  {
    animator = GetComponent<Animator>();
    trivia.OnNewQuestion.AddListener(NewQuestion);
    trivia.OnCorrect.AddListener(CorrectAnswer);
    trivia.OnIncorrect.AddListener(IncorrectAnswer);
    Clear();
  }

  void NewQuestion()
  {
    if ( animator != null )
    {
      animator.SetBool("showQuestion",true);
    }
    question.text = trivia.CurrentQuestion.question;
    for (int i = 0; i < answers.Length && i < trivia.Answers.Count; i++)
    {
      answers[i].text = trivia.Answers[i];
    }
  }

  void CorrectAnswer()
  {
    Debug.Log("CORRECT!");
    Clear();
    question.text = "Correct";
    Invoke("ClearAndHide", 1f);
  }

  void IncorrectAnswer()
  {
    Debug.Log("INCORRECT!");
    Clear();
    question.text = "Incorrect!";
    Invoke("ClearAndHide",1f);
  }

  public void Clear()
  {
    question.text = string.Empty;
    for (int i = 0; i < answers.Length; i++)
    {
      answers[i].text = string.Empty;
    }
  }

  public void ClearAndHide()
  {
    Clear();
    if (animator != null)
    {
      animator.SetBool("showQuestion", false);
    }
  }

}
