using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TriviaSetup : MonoBehaviour
{
  
  [SerializeField]
  private List<QuestionGate> gates;

  [SerializeField]
  private TMP_Text questionText;

  public void SetQuestion(Question q)
  {
    questionText.text = q.question;

    List<int> gateIndicies = new List<int>();
    for (int i = 0; i < gates.Count; ++i)
    {
      gateIndicies.Add(i);
    }

    int correct = Random.Range(0, gateIndicies.Count);
    gates[correct].isCorrect = true;
    gates[correct].SetAnswer(q.correct);
    gateIndicies.Remove(correct);
    gates[correct].answerSelected += Callback;
    gates[correct].gameObject.SetActive(true);

    for (int i = 1; i < gates.Count; ++i)
    {
      int wrong = gateIndicies[Random.Range(0, gateIndicies.Count)];
      gates[wrong].isCorrect = false;

      int lookup = wrong >= correct ? wrong - 1 : wrong;
      gates[wrong].SetAnswer(q.wrong[lookup]);
      gateIndicies.Remove(wrong);

      gates[wrong].answerSelected += Callback;
      gates[wrong].gameObject.SetActive(true);
    }
  }

  private void Callback()
  {
    foreach (var gate in gates)
    {
      gate.gameObject.SetActive(false);
      gate.answerSelected -= Callback;
    }
    gameObject.SetActive(false);
  }

}
