﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace GameJam.Utility
{
  public class Delay : MonoBehaviour
  {
    public float delay = 1f;

    public UnityEvent action;

    float time = 0;

    void OnEnable()
    {
      time = 0;
    }

    void Update()
    {
      time += Time.GameTime.DeltaTime;
      if (time >= delay)
      {
        action.Invoke();
        time = 0;
        this.enabled = false;
      }
    }

  }
}