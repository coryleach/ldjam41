﻿using UnityEngine;
using System.Collections;

namespace GameJam.Utility
{

  public class DontDestroyOnLoad : MonoBehaviour
  {
    void Awake()
    {
      DontDestroyOnLoad( this.gameObject );
    }
  }

}