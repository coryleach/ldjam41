﻿using UnityEngine;
using System.Collections;

namespace GameJam.Utility
{

  public class HideAtStart : MonoBehaviour
  {

    // Use this for initialization
    void Start()
    {
      this.gameObject.SetActive( false );
    }

  }

}