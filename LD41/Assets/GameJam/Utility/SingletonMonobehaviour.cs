﻿using UnityEngine;
using GameJam.Deferrable;

namespace GameJam.Utility
{
    public class SingletonMonobehaviour<T> : MonoBehaviour
    {
        static T _current;
        public static T Current
        {
            get
            {
                return _current;
            }
        }

        static Deferred initializationPromise = new Deferred();
        public static IPromise Initialized()
        {
            return initializationPromise;
        }

        protected virtual void Awake()
        {
            _current = gameObject.GetComponent<T>();
            initializationPromise.Complete();
        }
        
        protected virtual void OnDestroy()
        {
            initializationPromise = new Deferred();
        }

    }
}


