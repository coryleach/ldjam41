﻿using UnityEngine;
using System.Collections;

namespace GameJam.Utility
{
  public class Observable<T> 
  {
    public delegate void ChangedDelegate( Observable<T> observable );
    public event ChangedDelegate OnValueChanged;

    T _value;
    public T Value
    {
      get
      {
        return _value;
      }
      set
      {
        if ( !_value.Equals( value ) )
        {
          _value = value;
          if ( OnValueChanged != null )
          {
            OnValueChanged( this );
          }
        }
      }
    }
  }
}
