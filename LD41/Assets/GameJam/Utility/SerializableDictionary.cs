﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameJam.Utility
{
    public interface IKeyValuePair<K, V>
    {
        K Key { get; set; }
        V Value { get; set; }
    }

    /// <summary>
    /// SerializableDictionary is an abstract dictionary class that uses
    /// a private abstract list property to implement its serializable state
    /// while at the same time keeping an internal dictionary to implement dictionary operations
    /// </summary>
    /// <typeparam name="T">Key</typeparam>
    /// <typeparam name="U">Value</typeparam>
    public abstract class SerializableDictionary<T,U,V> : IDictionary<T, U> where V : IKeyValuePair<T,U>
    {
        
        protected abstract List<V> KeyValueList { get; }
        protected abstract V NewPair();

        private Dictionary<T, U> internalDict = new Dictionary<T, U>();
        private bool internalDictPopulated = false;

        public U this[T index]
        {
            get
            {
                return internalDict[index];
            }
            set
            {
                AddOrSetValue(index, value);   
            }
        }

        public int Count
        {
            get
            {
                return KeyValueList.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public ICollection<T> Keys
        {
            get
            {
                BuildDict();
                return internalDict.Keys;
            }
        }

        public ICollection<U> Values
        {
            get
            {
                BuildDict();
                return internalDict.Values;
            }
        }

        public void Clear()
        {
            internalDict.Clear();
            KeyValueList.Clear();
        }

        public void Add(T key, U value)
        {
            BuildDict();

            internalDict.Add(key, value);
            var pair = NewPair();
            pair.Key = key;
            pair.Value = value;
            KeyValueList.Add(pair);
        }

        public void Add(KeyValuePair<T,U> pair)
        {
            this.Add(pair.Key, pair.Value);
        }

        public bool Contains(KeyValuePair<T, U> pair)
        {
            BuildDict();
            return ((ICollection<KeyValuePair<T, U>>)internalDict).Contains(pair);
        }

        public bool Remove(KeyValuePair<T, U> pair)
        {
            return internalDict.Remove(pair.Key);
        }

        public void CopyTo(KeyValuePair<T,U>[] array, int index)
        {
            BuildDict();
            ((ICollection<KeyValuePair<T, U>>)internalDict).CopyTo(array, index);
        }

        public bool ContainsKey(T key)
        {
            BuildDict();
            return internalDict.ContainsKey(key);
        }

        public bool Remove(T key)
        {
            BuildDict();

            if ( internalDict.Remove(key) )
            {
                //Remove from list
                for ( int i = 0; i < KeyValueList.Count; i++ )
                {
                    var pair = KeyValueList[i];
                    if ( pair.Key.Equals(key) )
                    {
                        KeyValueList.RemoveAt(i);
                        return true;
                    }
                }
            }

            return false;
        }

        public bool TryGetValue(T key, out U value)
        {
            BuildDict();
            return internalDict.TryGetValue(key, out value);
        }

        public IEnumerator<KeyValuePair<T, U>> GetEnumerator()
        {
            BuildDict();
            return internalDict.GetEnumerator();// as IEnumerator<KeyValuePair<T,U>>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            BuildDict();
            return internalDict.GetEnumerator();
        }

        void BuildDict()
        {
            if ( internalDict.Count != KeyValueList.Count || !internalDictPopulated )
            {
                internalDict.Clear();
                for ( int i = 0; i < KeyValueList.Count; i++ )
                {
                    var pair = KeyValueList[i];
                    internalDict.Add(pair.Key, pair.Value);
                }
            }
            internalDictPopulated = true;
        }

        void AddOrSetValue(T key, U value)
        {
            BuildDict();

            if ( internalDict.ContainsKey(key) )
            {
                //Update existing value
                internalDict[key] = value;
                GetPair(key).Value = value;
            }
            else
            {
                //Add new value
                Add(key, value);
            }
        }

        IKeyValuePair<T,U> GetPair(T key)
        {
            for ( int i = 0; i < KeyValueList.Count; i++ )
            {
                var pair = KeyValueList[i];
                if ( pair.Key.Equals(key) )
                {
                    return pair;
                }
            }
            return null;
        }

    }
}


