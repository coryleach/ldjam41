﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using GameJam.UI;

namespace GameJam.Utility
{
  /// <summary>
  /// Put this component anywhere and call Load on it to load the specified scene
  /// This is useful for buttons and triggers
  /// </summary>
  public class LoadScene : MonoBehaviour
  {

    public string scene;

    public UnityEvent OnSceneLoaded = new UnityEvent();

    public bool useTransitionsIfAvailable = true;

    public void Load()
    {
      if (useTransitionsIfAvailable && SceneLoader.Current != null)
      {
        SceneLoader.Current.LoadScene(scene, () => { OnSceneLoaded.Invoke(); });
      }
      else
      {
        SceneManager.LoadScene(scene);
      }
    }

  }

}
