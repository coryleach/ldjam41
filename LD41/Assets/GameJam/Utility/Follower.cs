﻿using UnityEngine;
using System.Collections;

namespace GameJam.Utility
{

  public class Follower : MonoBehaviour
  {

    [SerializeField]
    Transform target;
    public Transform Target
    {
      get
      {
        return target;
      }
      set
      {
        target = value;
        this.enabled = true;
      }
    }

    public bool copyPosition = true;
    public bool copyRotation = false;

    void Update()
    {
      if (target == null)
      {
        this.enabled = false;
      }
      else
      {

        if (copyPosition)
        {
          transform.position = target.position;
        }

        if (copyRotation)
        {
          transform.rotation = target.rotation;
        }

      }
    }

  }

}