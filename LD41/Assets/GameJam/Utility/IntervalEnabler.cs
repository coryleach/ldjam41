﻿using UnityEngine;
using System.Collections;

namespace GameJam.Utility
{

  public class IntervalEnabler : MonoBehaviour
  {

    public float delay = 0f;
    public float interval = 0.6f;
    public GameObject[] objects;

    float time = 0;
    int count = 0;

    void OnEnable()
    {
      count = 0;
      time = delay;
    }

    // Update is called once per frame
    void Update()
    {
      if ( count < objects.Length )
      {

        if ( time <= 0 )
        {

          if ( objects[count] != null )
          {
            objects[count].SetActive(true);
          }
          
          time = interval;
          count += 1;

        }
        else
        {
          time -= UnityEngine.Time.deltaTime;
        }

      }
      else
      {
        enabled = false;
      }
    }

  }

}