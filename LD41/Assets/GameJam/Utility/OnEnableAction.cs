﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace GameJam.Utility
{

	public class OnEnableAction : MonoBehaviour 
	{

		public UnityEvent onEnable;

		void OnEnable()
		{
			onEnable.Invoke();
		}

	}

}