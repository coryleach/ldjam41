﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace GameJam.Utility
{

  public class TiggerExecute : MonoBehaviour
  {

    public UnityEvent OnEnter;
    public UnityEvent OnExit;

    void OnTriggerEnter( Collider collider )
    {
      OnEnter.Invoke();
    }

    void OnTriggerExit( Collider collider )
    {
      OnExit.Invoke();
    }

  }

}
