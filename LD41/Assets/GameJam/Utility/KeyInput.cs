﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace GameJam.Utility
{

  public class KeyInput : MonoBehaviour
  {

    public KeyCode key;

    public UnityEvent OnKeyDown;
    public UnityEvent OnKeyUp;

    void Update()
    {

      if ( Input.GetKeyDown( key ) )
      {
        OnKeyDown.Invoke();
      }

      if ( Input.GetKeyUp( key ) )
      {
        OnKeyUp.Invoke();
      }

    }

  }

}
