﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Utility
{


  /// <summary>
  /// ObjectCache uses a Least Recently Used policy to cache a limited set of key-value pairs
  /// </summary>
  public class ObjectCache<TKey, TValue>
  {

    protected class CacheEntry
    {
      public TKey key;
      public TValue value;
    }

    int maxSize = 20;
    public int MaxSize
    {
      get { return maxSize; }
      set { maxSize = value; }
    }

    LinkedList<CacheEntry> entryList = new LinkedList<CacheEntry>();
    Dictionary<TKey, LinkedListNode<CacheEntry>> entryIndex = new Dictionary<TKey, LinkedListNode<CacheEntry>>();

    public ObjectCache()
    {

    }

    public ObjectCache(int maxSize)
    {
      MaxSize = maxSize;
    }

    public bool TryGet(TKey key, out TValue value)
    {
      //Find the node and move it to the front
      var node = GetNode(key);
      if (node != null)
      {
        entryList.Remove(node);
        entryList.AddFirst(node);
        value = node.Value.value;
        return true;
      }
      else
      {
        value = default(TValue);
        return false;
      }
    }

    public bool ContainsKey(TKey key)
    {
      return entryIndex.ContainsKey(key);
    }

    public void Add(TKey key, TValue value)
    {

      if (entryIndex.ContainsKey(key))
      {
        throw new System.Exception("Key already exists in object cache");
      }

      var entry = new CacheEntry();
      entry.key = key;
      entry.value = value;

      //Add node to the front of the list
      var node = entryList.AddFirst(entry);

      //Add node to the entry index
      entryIndex.Add(key, node);

      if (entryList.Count > MaxSize)
      {
        RemoveLeastRecentlyUsed();
      }

    }

    public bool Remove(TKey key)
    {
      var node = GetNode(key);
      if (node != null)
      {
        entryList.Remove(node);
        return true;
      }
      else
      {
        return false;
      }
    }

    void RemoveLeastRecentlyUsed()
    {
      var lastNode = entryList.Last;
      entryList.RemoveLast();
      entryIndex.Remove(lastNode.Value.key);
    }

    LinkedListNode<CacheEntry> GetNode(TKey key)
    {
      LinkedListNode<CacheEntry> node = null;
      if (entryIndex.TryGetValue(key, out node))
      {
        return node;
      }
      return null;
    }

  }

}