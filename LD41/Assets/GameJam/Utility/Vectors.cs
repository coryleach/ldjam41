﻿using UnityEngine;
using System;
using System.Collections;

namespace GameJam.Utility
{

  public interface ICalculator<T>
  {
    T Sum( T a, T b );
    T Difference( T a, T b );
    int Compare( T a, T b );
    T Multiply( T a, T b );
    T Divide( T a, T b );
    T Divide( T a, int b );
  }

  //TODO: Make it so vectors can be multiplied by an integer

  struct Int32Calculator : ICalculator<Int32>
  {
    public Int32 Sum( Int32 a, Int32 b )
    {
      return a + b;
    }

    public Int32 Difference( Int32 a, Int32 b )
    {
      return a - b;
    }

    public int Compare( Int32 a, Int32 b )
    {
      return (int)Difference( a, b );
    }

    public int Multiply( Int32 a, Int32 b )
    {
      return a * b;
    }

    public int Divide( Int32 a, Int32 b )
    {
      return a / b;
    }
  }

  struct Int64Calculator : ICalculator<Int64>
  {
    public Int64 Sum( Int64 a, Int64 b )
    {
      return a + b;
    }

    public Int64 Difference( Int64 a, Int64 b )
    {
      return a - b;
    }

    public int Compare( Int64 a, Int64 b )
    {
      return (int)Difference( a, b );
    }

    public Int64 Multiply( Int64 a, Int64 b )
    {
      return a * b;
    }

    public Int64 Divide( Int64 a, Int64 b )
    {
      return a / b;
    }

    public Int64 Divide( Int64 a, Int32 b )
    {
      return (a / b);
    }
  }

  struct FloatCalculator : ICalculator<float>
  {
    public float Sum( float a, float b )
    {
      return a + b;
    }

    public float Difference( float a, float b )
    {
      return a - b;
    }

    public int Compare( float a, float b )
    {
      return ( int )Difference( a, b );
    }

    public float Multiply( float a, float b )
    {
      return a * b;
    }

    public float Divide( float a, float b )
    {
      return a / b;
    }

    public float Divide( float a, int b )
    {
      return ( a / b );
    }
  }

  struct DoubleCalculator : ICalculator<double>
  {
    public double Sum( double a, double b )
    {
      return a + b;
    }

    public double Difference( double a, double b )
    {
      return a - b;
    }

    public int Compare( double a, double b )
    {
      return ( int )Difference( a, b );
    }

    public double Multiply( double a, double b )
    {
      return a * b;
    }

    public double Divide( double a, double b )
    {
      return a / b;
    }

    public double Divide( double a, int b )
    {
      return ( a / b );
    }
  }

  public struct Vector2<T> where T : struct
  {

    public T x, y;

    public Vector2( T x, T y )
    {
      this.x = x;
      this.y = y;
    }
    
    public T this[ int index ]
    {
      get
      {
        switch ( index )
        {
          case 0:
          return x;
          case 1:
          return y;
          default:
          throw new IndexOutOfRangeException(string.Format("Index value {0} out of range for Vector2",index));
        }
      }
    }

    private static ICalculator<T> _calculator = null;
    public static ICalculator<T> Calculator
    {
      get
      {
        if ( _calculator == null )
        {
          MakeCalculator();
        }
        return _calculator;
      }
    }

    public static void MakeCalculator()
    {
      Type calculatorType = GetCalculatorType();
      _calculator = Activator.CreateInstance( calculatorType ) as ICalculator<T>;
    }

    public static Type GetCalculatorType()
    {
      Type tType = typeof( T );
      Type calculatorType = null;
      if ( tType == typeof( int ) )
      {
        calculatorType = typeof( Int32Calculator );
      }
      else if ( tType == typeof( long ) )
      {
        calculatorType = typeof( Int64Calculator );
      }
      else if ( tType == typeof( double ) )
      {
        calculatorType = typeof( DoubleCalculator );
      }
      else if ( tType == typeof( float ) )
      {
        calculatorType = typeof( FloatCalculator );
      }
      else
      {
        throw new InvalidCastException( String.Format( "Unsupported Vector Type- Type {0}", tType.Name ) );
      }
      return calculatorType;
    }

    public static Vector2<T> operator +( Vector2<T> a, Vector2<T> b )
    {
      return new Vector2<T>( Calculator.Sum(a.x,b.x), Calculator.Sum(a.y,b.y) );
    }

    public static Vector2<T> operator -( Vector2<T> a, Vector2<T> b )
    {
      return new Vector2<T>( Calculator.Difference(a.x,b.x), Calculator.Difference(a.y,b.y) );
    }

    public static Vector2<T> operator *( Vector2<T> a, Vector2<T> b )
    {
      return new Vector2<T>( Calculator.Multiply(a.x,b.x), Calculator.Multiply(a.y,b.y) );
    }

  }

  public struct Vector3<T> where T : struct
  {

    public T x, y, z;

    public Vector3( T x, T y, T z )
    {
      this.x = x;
      this.y = y;
      this.z = z;
    }

    public T this[ int index ]
    {
      get
      {
        switch ( index )
        {
          case 0:
          return x;
          case 1:
          return y;
          case 2:
          return z;
          default:
          throw new IndexOutOfRangeException( string.Format( "Index value {0} out of range for Vector3", index ) );
        }
      }
    }

    private static ICalculator<T> _calculator = null;
    public static ICalculator<T> Calculator
    {
      get
      {
        if ( _calculator == null )
        {
          MakeCalculator();
        }
        return _calculator;
      }
    }

    public static void MakeCalculator()
    {
      Type calculatorType = GetCalculatorType();
      _calculator = Activator.CreateInstance( calculatorType ) as ICalculator<T>;
    }

    public static Type GetCalculatorType()
    {
      Type tType = typeof( T );
      Type calculatorType = null;
      if ( tType == typeof( int ) )
      {
        calculatorType = typeof( Int32Calculator );
      }
      else if ( tType == typeof( long ) )
      {
        calculatorType = typeof( Int64Calculator );
      }
      else if ( tType == typeof( double ) )
      {
        calculatorType = typeof( DoubleCalculator );
      }
      else if ( tType == typeof( float ) )
      {
        calculatorType = typeof( FloatCalculator );
      }
      else
      {
        throw new InvalidCastException( String.Format( "Unsupported Type- Type {0}" +
              " does not have a partner implementation of interface " +
              "ICalculator<T> and cannot be used in generic " +
              "arithmetic using type Number<T>", tType.Name ) );
      }
      return calculatorType;
    }

    public static Vector3<T> operator +( Vector3<T> a, Vector3<T> b )
    {
      return new Vector3<T>( Calculator.Sum( a.x, b.x ), Calculator.Sum( a.y, b.y ), Calculator.Sum( a.z, b.z ) );
    }

    public static Vector3<T> operator -( Vector3<T> a, Vector3<T> b )
    {
      return new Vector3<T>( Calculator.Difference( a.x, b.x ), Calculator.Difference( a.y, b.y ), Calculator.Difference( a.z, b.z ) );
    }

    public static Vector3<T> operator *( Vector3<T> a, Vector3<T> b )
    {
      return new Vector3<T>( Calculator.Multiply( a.x, b.x ), Calculator.Multiply( a.y, b.y ), Calculator.Multiply( a.z, b.z ) );
    }


  }

}