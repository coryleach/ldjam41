﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameJam.Utility
{

  public class ObservableList<T> : IList<T>
  {

    List<T> _list = new List<T>();

    public delegate void ListUpdateHandler( object sender, object updatedValue );
    public delegate void ListClearHandler();

    public event ListUpdateHandler OnItemAdded;
    public event ListUpdateHandler OnItemRemoved;

    public event ListClearHandler OnCleared;

    #region IList Interface
    public int IndexOf( T value )
    {
      return _list.IndexOf( value );
    }

    public void Insert( int index, T value )
    {
      _list.Insert( index, value );
    }

    public void RemoveAt( int index )
    {
      _list.RemoveAt( index );
    }

    public T this[ int index ]
    {
      get
      {
        return _list[ index ];
      }
      set
      {
        _list[ index ] = value;
      }
    }
    #endregion

    #region IEnumerable implementation
    public IEnumerator<T> GetEnumerator()
    {
      return _list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }
    #endregion

    #region ICollection[T] implementation
    public void Add( T item )
    {
      if ( OnItemAdded != null )
      {
        OnItemAdded( this, item );
      }

      _list.Add( item );
    }

    public void Clear()
    {
      _list.Clear();

      if ( OnCleared != null )
      {
        OnCleared();
      }
    }

    public bool Contains( T item )
    {
      return _list.Contains( item );
    }

    public void CopyTo( T[] array, int arrayIndex )
    {
      _list.CopyTo( array, arrayIndex );
    }

    public bool Remove( T item )
    {
      if ( OnItemRemoved != null )
      {
        OnItemRemoved( this, item );
      }

      return _list.Remove( item );
    }

    public int Count
    {
      get
      {
        return _list.Count;
      }
    }

    public bool IsReadOnly
    {
      get
      {
        return false;
      }
    }
    #endregion

  }

}