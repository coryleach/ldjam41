﻿using UnityEngine;
using System;
using System.Collections;

namespace GameJam.Utility
{

	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class ComponentTypeRestrictionAttribute : PropertyAttribute
	{
	    public Type inheritsFromType;

	    public ComponentTypeRestrictionAttribute(Type _inheritsFromType)
	    {
	        inheritsFromType = _inheritsFromType;
	    }
	}

}