﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Utility
{

  public class Destroyer : MonoBehaviour
  {
    public void SelfDestruct()
    {
      Destroy(gameObject);
    }
  }

}
