﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Mesh
{

  using Mesh = UnityEngine.Mesh;

  public static class MeshUtility
  {

    /// <summary>
    /// Create a new Quad Mesh on the X-Y plane
    /// </summary>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    /// <returns></returns>
    public static Mesh CreateQuadMeshXY(float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      Mesh mesh = new Mesh();

      Vector3[] verts = null;
      Vector2[] uv = null;
      int[] tris = null;

      BuildQuadMeshXY(ref verts, ref uv, ref tris, totalWidth, totalHeight, quadsWide, quadsHigh);

      mesh.vertices = verts;
      mesh.uv = uv;
      mesh.SetTriangles(tris, 0);

      return mesh;
    }

    /// <summary>
    /// Create a new Quad Mesh on the X-Z plane
    /// </summary>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    /// <returns></returns>
    public static Mesh CreateQuadMeshXZ(float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      Mesh mesh = new Mesh();

      Vector3[] verts = null;
      Vector2[] uv = null;
      int[] tris = null;

      BuildQuadMeshXZ(ref verts, ref uv, ref tris, totalWidth, totalHeight, quadsWide, quadsHigh);

      mesh.vertices = verts;
      mesh.uv = uv;
      mesh.SetTriangles(tris, 0);

      return mesh;
    }

    /// <summary>
    /// Populates a mesh with verts and uvs for a quad
    /// Allocates arrays for verts and UVs to populate a mesh object
    /// Builds the mesh on the X-Y plane
    /// </summary>
    /// <param name="mesh"></param>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    public static void PopulateQuadMeshXY(Mesh mesh, float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      Vector3[] verts = null;
      Vector2[] uv = null;
      int[] tris = null;

      BuildQuadMeshXY(ref verts, ref uv, ref tris, totalWidth, totalHeight, quadsWide, quadsHigh);

      mesh.vertices = verts;
      mesh.uv = uv;
      mesh.SetTriangles(tris, 0);
    }

    /// <summary>
    /// Populates a mesh with verts and uvs for a quad
    /// Allocates arrays for verts and UVs to populate the mesh object
    /// Builds the mesh on the X-Z plane
    /// </summary>
    /// <param name="mesh"></param>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    public static void PopulateQuadMeshXZ(Mesh mesh, float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      Vector3[] verts = null;
      Vector2[] uv = null;
      int[] tris = null;

      BuildQuadMeshXZ(ref verts, ref uv, ref tris, totalWidth, totalHeight, quadsWide, quadsHigh);

      mesh.vertices = verts;
      mesh.uv = uv;
      mesh.SetTriangles(tris, 0);
    }

    /// <summary>
    /// Defaults to building a mesh on the x-y plane
    /// </summary>
    /// <param name="verts"></param>
    /// <param name="uv"></param>
    /// <param name="tris"></param>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    public static void BuildQuadMeshXY(ref Vector3[] verts, ref Vector2[] uv, ref int[] tris, float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      BuildQuadMesh(ref verts, ref uv, ref tris, Vector3.right, Vector3.up, totalWidth, totalHeight, quadsWide, quadsHigh);
    }

    /// <summary>
    /// Defaults to building a mesh on the x-z plane
    /// </summary>
    /// <param name="verts"></param>
    /// <param name="uv"></param>
    /// <param name="tris"></param>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    public static void BuildQuadMeshXZ(ref Vector3[] verts, ref Vector2[] uv, ref int[] tris, float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {
      BuildQuadMesh(ref verts, ref uv, ref tris, Vector3.right, Vector3.forward, totalWidth, totalHeight, quadsWide, quadsHigh);
    }

    /// <summary>
    /// Builds a quad mesh on the specified axis
    /// </summary>
    /// <param name="verts"></param>
    /// <param name="uv"></param>
    /// <param name="tris"></param>
    /// <param name="horizontal"></param>
    /// <param name="vertical"></param>
    /// <param name="totalWidth"></param>
    /// <param name="totalHeight"></param>
    /// <param name="quadsWide"></param>
    /// <param name="quadsHigh"></param>
    public static void BuildQuadMesh(ref Vector3[] verts, ref Vector2[] uv, ref int[] tris, Vector3 horizontal, Vector3 vertical, float totalWidth, float totalHeight, int quadsWide = 1, int quadsHigh = 1)
    {

      int totalQuads = quadsWide * quadsHigh;
      int vertCount = totalQuads * 4; //(quadsWide + 1) * (quadsHigh + 1); //Minimum number of verts required to build our mesh
      int triVertCount = totalQuads * 6; //6 triangle verts per quad because two triangles per quad

      int vertsPerRow = (quadsWide + 1);
      int vertsPerColumn = (quadsHigh + 1);

      if (verts == null || verts.Length != vertCount)
      {
        verts = new Vector3[vertCount];
      }

      if (uv == null || uv.Length != vertCount)
      {
        uv = new Vector2[vertCount];
      }

      if (tris == null || tris.Length != triVertCount)
      {
        tris = new int[triVertCount];
      }

      float quadWidth = totalWidth / quadsWide;
      float quadHeight = totalHeight / quadsHigh;

      for (int y = 0; y < quadsHigh; y++)
      {
        for (int x = 0; x < quadsWide; x++)
        {

          //(0,1)-----(1,1)
          //  |         | 
          //  |         |
          //(0,0)-----(1,0)

          //(0,2)-----(1,2)-----(2,2)
          //  |         |         |
          //  |         |         |
          //(0,1)-----(1,1)-----(2,1)
          //  |         |         |
          //  |         |         |
          //(0,0)-----(1,0)-----(2,0)

          //(0,3)-----(1,3)-----(2,3)-----(3,3)
          //  |         |         |         |
          //  |  0,2    |  1,2    |  2,2    |
          //(0,2)-----(1,2)-----(2,2)-----(3,2)
          //  |         |         |         |
          //  |  0,1    |  1,1    |  2,1    |
          //(0,1)-----(1,1)-----(2,1)-----(3,1)
          //  |         |         |         |
          //  |   0,0   |   1,0   |  2,0    |
          //(0,0)-----(1,0)-----(2,0)-----(3,0)

          int quadIndex = (y * quadsWide) + x;
          int triIndex = quadIndex * 6;

          float xPos = x * quadWidth;
          float yPos = y * quadHeight;

          int vertIndex = quadIndex * 4;

          verts[vertIndex] = (horizontal * xPos) + (vertical * yPos); //Lower Left
          verts[vertIndex + 1] = (xPos + quadWidth) * horizontal + (yPos * vertical); //Lower Right
          verts[vertIndex + 2] = (xPos * horizontal) + (yPos + quadHeight) * vertical;  //Upper Left
          verts[vertIndex + 3] = (xPos + quadWidth) * horizontal + (yPos + quadHeight) * vertical; //Upper Right

          uv[vertIndex] = new Vector2(0, 0);
          uv[vertIndex + 1] = new Vector2(1, 0);
          uv[vertIndex + 2] = new Vector2(0, 1);
          uv[vertIndex + 3] = new Vector2(1, 1);

          //Triangle 1
          tris[triIndex] = vertIndex;
          tris[triIndex + 1] = vertIndex + 2;
          tris[triIndex + 2] = vertIndex + 3;

          //Triangle 2
          tris[triIndex + 3] = vertIndex + 1;
          tris[triIndex + 4] = vertIndex;
          tris[triIndex + 5] = vertIndex + 3;

        }
      }

    }

  }

}