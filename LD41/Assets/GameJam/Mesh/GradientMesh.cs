﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Mesh
{

  [RequireComponent(typeof(MeshRenderer))]
  [RequireComponent(typeof(MeshFilter))]
  public class GradientMesh : MonoBehaviour
  {

    Vector3[] verts = null;
    Vector2[] UVs = null;
    int[] tris = null;

    public Color topColor;
    public Color bottomColor;

    // Use this for initialization
    void Start()
    {
      UnityEngine.Mesh mesh = new UnityEngine.Mesh();

      MeshUtility.BuildQuadMeshXY(ref verts, ref UVs, ref tris, 1, 1, 1, 1);

      var colors = new List<Color>();//[verts.Length];

      //Color the vertex based on the y position
      for ( int i = 0; i < verts.Length; i++)
      {
        colors.Add(Color.Lerp(bottomColor,topColor, verts[i].y));
      }

    }

  }

}
