﻿using System;

namespace GameJam.Deferrable
{

  public interface IPromise 
  {

    bool IsComplete { get; }

    IPromise Then(Action callback);
    IPromise ThenWaitFor(Func<IPromise> callback);
    IPromise<TNextValue> ThenWaitFor<TNextValue>(Func<IPromise<TNextValue>> callback);
    IPromise Always(Action callback);
    IPromise Catch(Action<Exception> callback);

  }

  public interface IPromise<PromisedType>
  {

    bool IsComplete { get; }

    IPromise<PromisedType> Then(Action<PromisedType> callback );
    IPromise ThenWaitFor(Func<PromisedType, IPromise> callback);
    IPromise<TNextValue> ThenWaitFor<TNextValue>(Func<PromisedType, IPromise<TNextValue>> callback);
    IPromise<PromisedType> Always(Action callback);
    IPromise<PromisedType> Catch( Action<Exception> callback );

  }

}
