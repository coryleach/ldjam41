﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Deferrable
{

  /// <summary>
  /// Deferred serves as a class that manages callbacks that should
  /// be deferred until some action has succeeded or failed.
  /// Deferred calls should pass back references to an internal promise that can be used
  /// to enqueue callbacks that will execute when the Deferred action is complete
  /// </summary>
  public class Deferred : IPromise
  {

    private bool _isComplete = false;
    private bool _isFailed = false;
    private List<PromiseCallback> callbacks = new List<PromiseCallback>();

    //Internal promise that will maintain a chain of callbacks
    public IPromise Promise { get { return this; } }

    public Deferred()
    {
      
    }

    public void Complete()
    {
      _isComplete = true;
      DequeueCallbacks();
    }

    public void Fail(Exception exception)
    {
      
      FailCallbacks(exception);
      AlwaysCallbacks();
    }

    void DequeueCallbacks()
    {
      try
      {
        ThenCallbacks();
      }
      catch (Exception e)
      {
        FailCallbacks(e);
      }
      finally
      {
        AlwaysCallbacks();
        callbacks.Clear();
      }
    }

    void ThenCallbacks()
    {
      //This should be success/fail callbacks
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];

        if (callback.Cond == PromiseCallback.Condition.Then)
        {
          callback.Del.DynamicInvoke();
        }
      }
    }

    void FailCallbacks(Exception exception)
    {
      _isFailed = true;
      //Catch callbacks in case of exception in any callback
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];
        if (callback.Cond == PromiseCallback.Condition.Catch)
        {
          callback.Del.DynamicInvoke(exception);
        }
      }
    }

    void AlwaysCallbacks()
    {
      //Catch callbacks in case of exception in any callback
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];
        if (callback.Cond == PromiseCallback.Condition.Always)
        {
          callback.Del.DynamicInvoke();
        }
      }
    }

    #region IPromise Interface 

    public bool IsComplete
    {
      get
      {
        return _isComplete || _isFailed;
      }
    }

    public IPromise Then(Action callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");

      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Then));
      
      return this;
    }

    public IPromise ThenWaitFor(Func<IPromise> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");

      Deferred convertedPromise = new Deferred();

      Action onResult = () =>
      {
        callback.Invoke().Then(() => { convertedPromise.Complete(); });
      };

      Action<Exception> onFail = (Exception exception) =>
      {
        convertedPromise.Fail(exception);
      };

      var promiseCallback = new PromiseCallback(onResult, PromiseCallback.Condition.Then);
      var errorCallback = new PromiseCallback(onFail, PromiseCallback.Condition.Catch);

      callbacks.Add(promiseCallback);
      callbacks.Add(errorCallback);

      return convertedPromise;
    }

    public IPromise<TNextValue> ThenWaitFor<TNextValue>(Func<IPromise<TNextValue>> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");
     
      Deferred<TNextValue> convertedPromise = new Deferred<TNextValue>();

      Action onResult = () =>
      {
        callback.Invoke().Then((TNextValue result) => { convertedPromise.Complete(result); });
      };

      Action<Exception> onFail = (Exception exception) =>
      {
        convertedPromise.Fail(exception);
      };

      var promiseCallback = new PromiseCallback(onResult, PromiseCallback.Condition.Then);
      var errorCallback = new PromiseCallback(onFail, PromiseCallback.Condition.Catch);

      callbacks.Add(promiseCallback);
      callbacks.Add(errorCallback);

      return convertedPromise;
    }

    public IPromise Catch(Action<Exception> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");
      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Catch));
      return this;
    }

    public IPromise Always(Action callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");
      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Always));
      return this;
    }

    #endregion

    class PromiseCallback
    {
      public enum Condition { Then, Catch, Always };

      public PromiseCallback(Delegate del, Condition cond)
      {
        Del = del;
        Cond = cond;
      }

      //public bool IsReturnValue { get; private set; }
      public Delegate Del { get; private set; }
      public Condition Cond { get; private set; }
    }

  }

  public class Deferred<TReturnValue> : IPromise<TReturnValue>
  {
    private bool _isComplete = false;
    private bool _isFailed = false;
    private List<PromiseCallback> callbacks = new List<PromiseCallback>();

    //Internal promise that will maintain a chain of callbacks
    public IPromise<TReturnValue> Promise { get { return this; } }

    public Deferred()
    {

    }

    public void Complete(TReturnValue value)
    {
      _isComplete = true;
      DequeueCallbacks(value);
    }

    public void Fail(Exception exception)
    {
      FailCallbacks(exception);
      AlwaysCallbacks();
    }

    void DequeueCallbacks(TReturnValue value)
    {
      try
      {
        ThenCallbacks(value);
      }
      catch (Exception e)
      {
        FailCallbacks(e);
      }
      finally
      {
        AlwaysCallbacks();
        callbacks.Clear();
      }
    }

    void ThenCallbacks(TReturnValue value)
    {
      //This should be success/fail callbacks
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];

        if (callback.Cond == PromiseCallback.Condition.Then)
        {
          callback.Del.DynamicInvoke(value);
        }
      }
    }

    void FailCallbacks(Exception exception)
    {
      _isFailed = true;
      //Catch callbacks in case of exception in any callback
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];
        if (callback.Cond == PromiseCallback.Condition.Catch)
        {
          callback.Del.DynamicInvoke(exception);
        }
      }
    }

    void AlwaysCallbacks()
    {
      //Catch callbacks in case of exception in any callback
      for (int i = 0; i < callbacks.Count; i++)
      {
        var callback = callbacks[i];
        if (callback.Cond == PromiseCallback.Condition.Always)
        {
          callback.Del.DynamicInvoke();
        }
      }
    }

    #region IPromise Interface 

    public bool IsComplete
    {
      get
      {
        return _isComplete || _isFailed;
      }
    }

    public IPromise<TReturnValue> Then(Action<TReturnValue> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");

      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Then));

      return this;
    }

    public IPromise ThenWaitFor(Func<TReturnValue,IPromise> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");

      Deferred convertedPromise = new Deferred();

      Action<TReturnValue> onResult = (value) =>
      {
        callback.Invoke(value).Then(() => { convertedPromise.Complete(); });
      };

      Action<Exception> onFail = (Exception exception) =>
      {
        convertedPromise.Fail(exception);
      };

      var promiseCallback = new PromiseCallback(onResult, PromiseCallback.Condition.Then);
      var errorCallback = new PromiseCallback(onFail, PromiseCallback.Condition.Catch);

      callbacks.Add(promiseCallback);
      callbacks.Add(errorCallback);

      return convertedPromise;
    }

    public IPromise<TNextValue> ThenWaitFor<TNextValue>(Func<TReturnValue,IPromise<TNextValue>> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");

      Deferred<TNextValue> convertedPromise = new Deferred<TNextValue>();

      Action<TReturnValue> onResult = (value) =>
      {
        callback.Invoke(value).Then((TNextValue result) => { convertedPromise.Complete(result); });
      };

      Action<Exception> onFail = (Exception exception) =>
      {
        convertedPromise.Fail(exception);
      };

      var promiseCallback = new PromiseCallback(onResult, PromiseCallback.Condition.Then);
      var errorCallback = new PromiseCallback(onFail, PromiseCallback.Condition.Catch);

      callbacks.Add(promiseCallback);
      callbacks.Add(errorCallback);

      return convertedPromise;
    }

    public IPromise<TReturnValue> Catch(Action<Exception> callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");
      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Catch));
      return this;
    }

    public IPromise<TReturnValue> Always(Action callback)
    {
      Debug.Assert(callback != null, "Promise callback cannot be null.");
      Debug.Assert(!_isComplete && !_isFailed, "Promise has already completed.");
      callbacks.Add(new PromiseCallback(callback, PromiseCallback.Condition.Always));
      return this;
    }

    #endregion

    class PromiseCallback
    {
      public enum Condition { Then, Catch, Always };

      public PromiseCallback(Delegate del, Condition cond)
      {
        Del = del;
        Cond = cond;
      }

      public Delegate Del { get; private set; }
      public Condition Cond { get; private set; }
    }

  }


}