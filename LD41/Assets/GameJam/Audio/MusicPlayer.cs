﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Services;

namespace GameJam.Audio
{

  public class MusicPlayer : MonoBehaviour
  {

    static MusicPlayer current;
    public static MusicPlayer Current
    {
      get
      {
        return ServiceProvider.Current.Get<MusicPlayer>();
      }
    }

    Dictionary<string, AudioSource> sourceDictionary = new Dictionary<string, AudioSource>();

    void Awake()
    {
      ServiceCollection.Current.Add(this);
    }

    void Start()
    {
      FindAudioSources();
    }

    void FindAudioSources()
    {

      sourceDictionary.Clear();

      //Get all sources in children
      var sources = GetComponentsInChildren<AudioSource>();
      foreach (var source in sources)
      {
        sourceDictionary.Add(source.name, source);
      }

    }

    public static void Play(string playlistName)
    {
      var playlists = Current.GetComponentsInChildren<MusicPlaylist>();
      foreach (var playlist in playlists)
      {
        if (playlist.name.Equals(playlistName))
        {
          playlist.Play();
        }
        else
        {
          //Stop other playlists?

        }
      }
    }

    public static void Stop(string playlistName)
    {
      var playlists = Current.GetComponentsInChildren<MusicPlaylist>();
      foreach (var playlist in playlists)
      {
        if (playlist.name.Equals(playlistName))
        {
          playlist.Stop();
        }
        else
        {

        }
      }
    }

  }

}
