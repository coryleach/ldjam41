﻿using UnityEngine;

namespace GameJam.Audio
{

  public class MusicPlayOnEnable : MonoBehaviour
  {

    public string playlistName;

    void OnEnable()
    {
      MusicPlayer.Play(playlistName);
    }

    void OnDisable()
    {
      MusicPlayer.Stop(playlistName);
    }

  }

}