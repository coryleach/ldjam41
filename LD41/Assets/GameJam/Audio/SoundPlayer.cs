﻿using System.Collections.Generic;
using UnityEngine;
using GameJam.Services;

namespace GameJam.Audio
{

  [RequireComponent(typeof(AudioSource))]
  public class SoundPlayer : MonoBehaviour
  {

    public static SoundPlayer Current
    {
      get
      {
        return ServiceProvider.Current.Get<SoundPlayer>();
      }
    }

    Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();

    AudioSource audioSource;
    public AudioSource AudioSource
    {
      get
      {
        if (audioSource == null)
        {
          audioSource = GetComponent<AudioSource>();
        }
        return audioSource;
      }
    }

    void Awake()
    {
      ServiceCollection.Current.Add(this);
    }

    void Start()
    {
      BuildDictionary();
    }

    void BuildDictionary()
    {
      sounds.Clear();

      var sources = GetComponentsInChildren<AudioSource>();
      foreach (var source in sources)
      {
        sounds.Add(source.name, source);
      }
    }

    public static void Play(string sfxName)
    {
      Current.AudioSource.PlayOneShot(Current.sounds[sfxName].clip);
    }

  }

}