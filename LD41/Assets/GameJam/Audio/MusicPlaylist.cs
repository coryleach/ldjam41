﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Audio
{

  public class MusicPlaylist : MonoBehaviour
  {

    [SerializeField]
    List<AudioSource> tracks = new List<AudioSource>();

    [SerializeField]
    bool looping = false;
    public bool Looping
    {
      get { return looping; }
      set { looping = value; }
    }

    public int Count
    {
      get { return tracks.Count; }
    }

    int index = 0;
    public int Index
    {
      get { return index; }
    }

    [ContextMenu("Play")]
    public void Play()
    {
      if (!playing)
      {
        StartTrack(index);
      }
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
      StopTrack(index);
    }

    [ContextMenu("Next")]
    public void Next()
    {

      if (playing)
      {
        StopTrack(index);
      }

      index += 1;
      if (index >= tracks.Count)
      {

        index = 0;

        if (!Looping)
        {
          playing = false;
          return;
        }

      }

      StartTrack(index);

    }

    [ContextMenu("Prev")]
    public void Prev()
    {
      if (playing)
      {
        StopTrack(index);
      }

      index -= 1;
      if (index < 0)
      {
        index = 0;
        playing = false;
      }
      else
      {
        StartTrack(index);
      }
    }

    [ContextMenu("Reset")]
    public void Reset()
    {
      if (playing)
      {
        Stop();
      }
      index = 0;
    }

    bool playing = false;

    void Update()
    {

      if (!playing)
      {
        enabled = false;
        return;
      }

      var audioSource = tracks[index];

      if (audioSource == null)
      {
        Next();
        return;
      }

      if (!audioSource.isPlaying || audioSource.time >= audioSource.clip.length)
      {
        Next();
      }

    }

    void StartTrack(int track)
    {
      playing = true;
      enabled = true;
      index = track;
      var audioSource = tracks[index];
      if (audioSource != null)
      {
        audioSource.Play();
      }
    }

    void StopTrack(int track)
    {
      playing = false;
      var audioSource = tracks[index];
      if (audioSource != null)
      {
        audioSource.Stop();
      }
    }

  }

}
