﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.Audio
{

  public class ButtonSfx : MonoBehaviour
  {

    Button button;
    public Button Button
    {
      get
      {
        if (button == null)
        {
          button = GetComponent<Button>();
        }
        return button;
      }
    }

    Toggle toggle;
    public Toggle Toggle
    {
      get
      {
        if (toggle == null)
        {
          toggle = GetComponent<Toggle>();
        }
        return toggle;
      }
    }

    void Start()
    {
      if (Button != null)
      {
        Button.onClick.AddListener(Click);
      }

      if (Toggle != null)
      {
        Toggle.onValueChanged.AddListener(ToggleValueChanged);
      }
    }

    void ToggleValueChanged(bool isOn)
    {
      if (isOn)
      {
        Click();
      }
    }

    void Click()
    {
      SoundPlayer.Play("ButtonClick");
    }

  }

}