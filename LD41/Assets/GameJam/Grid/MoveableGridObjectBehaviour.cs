﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{
  public class MoveableGridObjectBehaviour : MonoBehaviour
  {

    IMoveableGridObject gridObject;
    public IMoveableGridObject GridObject
    {
      get
      {
        if (gridObject == null)
        {
          gridObject = GetComponent<IMoveableGridObject>();
        }
        return gridObject;
      }
    }

  }
}