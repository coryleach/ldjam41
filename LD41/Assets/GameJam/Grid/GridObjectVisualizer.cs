﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{
  /// <summary>
  /// Draws grid gizmos for visualizing grid objects in a scene
  /// </summary>
  public class GridObjectVisualizer : GridObjectBehaviour
  {
    public Color color;

    void OnDrawGizmos()
    {
      Gizmos.color = color;
      Gizmos.DrawCube(transform.position + GridObject.Location.Center,GridObject.Location.VectorSize);
    }
    
  }

}