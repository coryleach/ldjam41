﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  [System.Serializable]
  public struct GridSize
  {

    public int w;
    public int h;

    public GridSize(int width, int height)
    {
      w = width;
      h = height;
    }

  }

}


