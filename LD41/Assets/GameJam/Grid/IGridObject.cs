﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  public interface IGridObject
  {
    GridRect Location { get; }
    GridSize Size { get; }
  }

}
