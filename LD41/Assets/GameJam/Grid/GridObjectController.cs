﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  /// <summary>
  /// Used to control the behavior of objects located on a grid
  /// </summary>
  public class GridObjectController : MonoBehaviour, IMoveableGridObject
  {

    [SerializeField]
    private GridMapController gridMap;
    public GridMapController GridMap
    {
      get
      {
        if ( gridMap == null )
        {
          gridMap = transform.GetComponentInParent<GridMapController>();
        }
        return gridMap;
      }
      set
      {
        gridMap = value;
      }
    }

    [SerializeField]
    private GridRect location;
    public GridRect Location
    {
      get { return location; }
    }

    public GridSize Size
    {
      get { return location.Size; }
    }

    public void MoveTo(int x, int y)
    {
      location.x = x;
      location.y = y;
      transform.localPosition = new Vector3(location.x, transform.localPosition.y, location.y);
    }

    void Start()
    {
      transform.localPosition = new Vector3(location.x, transform.localPosition.y, location.y);
    }

    void OnEnable()
    {
      GridMap.AddGridObject(this);
    }

    void OnDisable()
    {
      GridMap.RemoveGridObject(this);
    }

  }

}