﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  using Mesh = UnityEngine.Mesh;

  [ExecuteInEditMode]
  [RequireComponent(typeof(MeshRenderer))]
  [RequireComponent(typeof(MeshFilter))]
  public class GridObjectMeshBuilder : GridObjectBehaviour
  {

    Vector3 offset = Vector3.zero;

    [SerializeField]
    int quadsWide = 1;

    [SerializeField]
    int quadsHigh = 1;

    Mesh mesh = null;
    public Mesh Mesh
    {
      get
      {
        if ( mesh == null )
        {
          mesh = new Mesh();
        }
        return mesh;
      }
    }

    MeshFilter meshFilter;
    public MeshFilter MeshFilter
    {
      get
      {
        if ( meshFilter == null )
        {
          meshFilter = GetComponent<MeshFilter>();
        }
        return meshFilter;
      }
    }

    Vector3[] verts = null;
    Vector2[] uv = null;
    int[] tris = null;

    void OnEnable()
    {
      BuildMesh();
    }

    void OnValidate()
    {
      if ( quadsWide < 1 )
      {
        quadsWide = 1;
      }

      if ( quadsHigh < 1 )
      {
        quadsHigh = 1;
      }

      BuildMesh();
    }

    [ContextMenu("BuildMesh")]
    void BuildMesh()
    {

      if ( mesh == null )
      {
        mesh = new Mesh();
      }
      else
      {
        mesh.Clear();
      }

      int totalQuads = quadsWide * quadsHigh;
      int vertCount = totalQuads * 4; //(quadsWide + 1) * (quadsHigh + 1); //Minimum number of verts required to build our mesh
      int triVertCount = totalQuads * 6; //6 triangle verts per quad because two triangles per quad

      int vertsPerRow = (quadsWide + 1);
      int vertsPerColumn = (quadsHigh + 1);

      float quadWidth = GridObject.Location.w / (float)quadsWide;
      float quadHeight = GridObject.Location.h / (float)quadsHigh;

      //Create mesh vertex array
      if ( verts == null || verts.Length != vertCount )
      {
        verts = new Vector3[vertCount];
      }
      
      if ( uv == null || uv.Length != vertCount )
      {
        uv = new Vector2[vertCount];
      }

      //Create triangle index array
      if ( tris == null || tris.Length != triVertCount )
      {
        tris = new int[triVertCount];
      }

      for ( int y = 0; y < quadsHigh; y++ )
      {
        for ( int x = 0; x < quadsWide; x++ )
        {

          //(0,1)-----(1,1)
          //  |         | 
          //  |         |
          //(0,0)-----(1,0)

          //(0,2)-----(1,2)-----(2,2)
          //  |         |         |
          //  |         |         |
          //(0,1)-----(1,1)-----(2,1)
          //  |         |         |
          //  |         |         |
          //(0,0)-----(1,0)-----(2,0)

          //(0,3)-----(1,3)-----(2,3)-----(3,3)
          //  |         |         |         |
          //  |  0,2    |  1,2    |  2,2    |
          //(0,2)-----(1,2)-----(2,2)-----(3,2)
          //  |         |         |         |
          //  |  0,1    |  1,1    |  2,1    |
          //(0,1)-----(1,1)-----(2,1)-----(3,1)
          //  |         |         |         |
          //  |   0,0   |   1,0   |  2,0    |
          //(0,0)-----(1,0)-----(2,0)-----(3,0)

          int quadIndex = (y * quadsWide) + x;
          int triIndex = quadIndex * 6;

          float xPos = x * quadWidth;
          float yPos = y * quadHeight;

          int vertIndex = quadIndex * 4;

          verts[vertIndex] = new Vector3(xPos, 0, yPos); //Lower Left
          verts[vertIndex + 1] = new Vector3(xPos + quadWidth, 0, yPos); //Lower Right
          verts[vertIndex + 2] = new Vector3(xPos, 0, yPos + quadHeight); //Upper Left
          verts[vertIndex + 3] = new Vector3(xPos + quadWidth, 0, yPos + quadHeight); //Upper Right

          uv[vertIndex] = new Vector2(0, 0);
          uv[vertIndex + 1] = new Vector2(1, 0);
          uv[vertIndex + 2] = new Vector2(0, 1);
          uv[vertIndex + 3] = new Vector2(1, 1);

          //Triangle 1
          tris[triIndex] = vertIndex;
          tris[triIndex + 1] = vertIndex + 2;
          tris[triIndex + 2] = vertIndex + 3;

          //Triangle 2
          tris[triIndex + 3] = vertIndex + 1;
          tris[triIndex + 4] = vertIndex;
          tris[triIndex + 5] = vertIndex + 3;//*/

          /*int lowerIndex = y * vertsPerRow + x;
          int higherIndex = (y + 1) * vertsPerRow + x;

          //Lower index verts only need to be set if we're on the first row
          if (y == 0)
          {
            verts[lowerIndex] = new Vector3(xPos, 0, yPos);
            verts[lowerIndex + 1] = new Vector3(xPos + quadWidth, 0, yPos);

            uv[lowerIndex] = new Vector2(0, 0);
            uv[lowerIndex + 1] = new Vector2(1,0);
          }

          verts[higherIndex] = new Vector3(xPos, 0, yPos + quadHeight);
          verts[higherIndex + 1] = new Vector3(xPos + quadWidth, 0, yPos + quadHeight);

          uv[higherIndex] = new Vector2(0, 1);
          uv[higherIndex + 1] = new Vector2(1, 1);

          //Triangle 1
          tris[triIndex] = lowerIndex;
          tris[triIndex + 1] = higherIndex;
          tris[triIndex + 2] = higherIndex + 1;

          //Triangle 2
          tris[triIndex + 3] = lowerIndex + 1;
          tris[triIndex + 4] = lowerIndex;
          tris[triIndex + 5] = higherIndex + 1;//*/

        }
      }

      mesh.vertices = verts;
      mesh.uv = uv;
      mesh.SetTriangles(tris, 0);
      MeshFilter.mesh = mesh;
    }
    
  }

}