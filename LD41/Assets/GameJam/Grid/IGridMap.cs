﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  public interface IGridMap
  {
    GridSize Size { get; }
  }

}
