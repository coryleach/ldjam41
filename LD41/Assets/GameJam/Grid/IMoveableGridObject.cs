﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{
  public interface IMoveableGridObject : IGridObject
  {
    void MoveTo(int x, int y);
  }
}