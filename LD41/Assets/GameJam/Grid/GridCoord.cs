﻿using System;

[Serializable]
public struct GridCoord
{

  public GridCoord(int _x, int _y)
  {
    x = _x;
    y = _y;
  }

  public int x;
  public int y;

}
