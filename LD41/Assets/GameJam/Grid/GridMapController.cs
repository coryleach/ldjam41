﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  public abstract class GridMapController : MonoBehaviour, IGridMap, IGridObject
  {

    public abstract GridSize Size { get; }

    public GridRect Location
    {
      get { return new GridRect(0, 0, Size.w, Size.h); }
    }

    protected List<IGridObject> gridObjects = new List<IGridObject>();

    public virtual GridRect ClampLocation(GridRect rect)
    {
      if ( rect.x < 0 )
      {
        rect.x = 0;
      }

      if ( rect.y < 0 )
      {
        rect.y = 0;
      }

      var mapSize = Size;

      if ( (rect.x + rect.w) > mapSize.w )
      {
        rect.x = mapSize.w - rect.w;
      }

      if ( (rect.y + rect.h) > mapSize.h )
      {
        rect.y = mapSize.h - rect.h;
      }

      return rect;
    }

    public virtual bool LocationIsValid(GridRect rect)
    {
      var mapSize = Size;
      if ( rect.x < 0 || rect.y < 0 || (rect.x + rect.w) > mapSize.w || (rect.y + rect.h) > mapSize.h )
      {
        return false;
      }

      //Make sure we don't overlap with any other grid objects
      for ( int i = 0; i < gridObjects.Count; i++ )
      {
        if ( gridObjects[i].Location.Intersects(rect) )
        {
          return false;
        }
      }

      return true;
    }

    public virtual void AddGridObject(IGridObject gridObject)
    {
      gridObjects.Add(gridObject);
    }

    public virtual void RemoveGridObject(IGridObject gridObject)
    {
      gridObjects.Remove(gridObject);
    }

  }

}
