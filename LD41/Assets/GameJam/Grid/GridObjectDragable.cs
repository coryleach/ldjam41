﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameJam.Grid
{

  public class GridObjectDragable : MoveableGridObjectBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
  {

    public UnityEvent OnDragStart = new UnityEvent();
    public UnityEvent OnDragPositionChange = new UnityEvent();
    public UnityEvent OnDragEnd = new UnityEvent();

    GridMapController _grid;
    public GridMapController Grid
    {
      get
      {
        if (_grid == null )
        {
          _grid = transform.GetComponentInParent<GridMapController>();
        }
        return _grid;
      }
    }

    Vector3 startPosition;
    Vector3 lastDragPosition;

    Vector3 dragStart;

    void Start()
    {
    }

    #region Drag Handling

    public void OnBeginDrag(PointerEventData eventData)
    {
      startPosition = transform.localPosition;
      lastDragPosition = transform.localPosition;

      var ray = eventData.pressEventCamera.ScreenPointToRay(eventData.position);
      var x = ray.origin.y / ray.direction.y * -1;
      dragStart = ray.origin + ray.direction * x;

      Grid.RemoveGridObject(GridObject);

      OnDragStart.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
      var ray = eventData.pressEventCamera.ScreenPointToRay(eventData.position);
      //Current WorldPoint at y = 0
      var x = ray.origin.y / ray.direction.y * -1;
      var dragCurrent = ray.origin + ray.direction * x;

      var dragDelta = (dragCurrent - dragStart);

      var pt = startPosition + dragDelta;
      
      pt.y = startPosition.y;
      pt.x = Mathf.Round(pt.x);
      pt.z = Mathf.Round(pt.z);

      if (lastDragPosition != pt)
      {
        lastDragPosition = pt;
        PositionChanged();
      }

    }

    public void OnEndDrag(PointerEventData eventData)
    {
      if ( Grid == null || !Grid.LocationIsValid(GridObject.Location) )
      {
        lastDragPosition = startPosition;
        PositionChanged();
      }
      else
      {

      }
      Grid.AddGridObject(GridObject);
      OnDragEnd.Invoke();
    }

    #endregion

    void PositionChanged()
    {
      //Everytime the transform actually moves from one grid cell to the next we need to update our grid rect
      GridObject.MoveTo((int)lastDragPosition.x,(int)lastDragPosition.z);
      var loc = Grid.ClampLocation(GridObject.Location);
      GridObject.MoveTo(loc.x,loc.y);
      OnDragPositionChange.Invoke();
    }

  }

}