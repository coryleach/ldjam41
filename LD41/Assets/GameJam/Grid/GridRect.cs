﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{
  /// <summary>
  /// Represents a grid location
  /// Grid locations are always represented as rectangles and never as points
  /// </summary>
  [System.Serializable]
  public struct GridRect
  {

    public int x;
    public int y;
    public int w;
    public int h;

    public GridRect(int x, int y, int w, int h)
    {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
    }

    public GridSize Size
    {
      get
      {
        return new GridSize(w, h);
      }
      set
      {
        w = value.w;
        h = value.h;
      }
    }

    public Vector3 VectorSize
    {
      get { return new Vector3(w, 0, h); }
    }

    public Vector3 Center
    {
      get { return new Vector3(w * 0.5f, 0,h * 0.5f); }
    }

    public bool Contains(GridRect rect)
    {
      if ( (rect.x >= x) && 
          ((rect.x+rect.w) <= (x + w)) && 
          (rect.y >= y) && 
          ((rect.y + rect.h) <= (y + h)) )
      {
        return true;
      }
      return false;
    }

    public bool Intersects(GridRect rect)
    {
      //Check if other rect's max is right of our min
      //This means we're not t0o far to the left
      if ( x < rect.x+rect.w )
      {
        //Check if other rect's minX is less than our max
        //This means we're not too far to the right
        if ( x+w > rect.x )
        {
          //Check if other rect's max is greater than our min
          //This means we wont be too far to the bottom
          if ( y < rect.y+rect.h )
          {
            //Check that the other rect's min is lower than our max
            //This means we're not too far to the top
            if ( y+h > rect.y)
            {
              return true;
            }
          }
        }
      }
      return false;
    }

  }

}