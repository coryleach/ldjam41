﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  /// <summary>
  /// Script is used for adding a box collider to any grid object
  /// </summary>
  [ExecuteInEditMode]
  [RequireComponent(typeof(BoxCollider))]
  public class GridObjectCollider : GridObjectBehaviour
  {

    BoxCollider _collider;
    public BoxCollider Collider
    {
      get
      {
        if (_collider == null )
        {
          _collider = GetComponent<BoxCollider>();
        }
        return _collider;
      }
    }

    [SerializeField]
    float scale = 1f;
    public float Scale
    {
      get
      {
        return scale;
      }
      set
      {
        scale = value;
        Refresh();
      }
    }

    void OnEnable()
    {
      Refresh();
    }

    [ContextMenu("Resize")]
    void Refresh()
    {
      if ( GridObject != null )
      {
        Collider.center = GridObject.Location.Center;
        Collider.size = GridObject.Location.VectorSize * scale;
      }
    }

    void OnValidate()
    {
      Refresh();
    }

  }

}