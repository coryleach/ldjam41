﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Grid
{

  /// <summary>
  /// Serves as a base class for any behaviour that needs to interact with the grid object it is attached to
  /// </summary>
  public class GridObjectBehaviour : MonoBehaviour
  {

    IGridObject gridObject;
    public IGridObject GridObject
    {
      get
      {
        if ( gridObject == null )
        {
          gridObject = GetComponent<IGridObject>();
        }
        return gridObject;
      }
    }
    
  }

}
