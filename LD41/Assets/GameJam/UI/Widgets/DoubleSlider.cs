﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoubleSlider : MonoBehaviour
{

  public Slider minSlider;
  public Slider maxSlider;
  public RectTransform fill;

  [SerializeField]
  bool wholeNumbers = false;
  public bool WholeNumbers
  {
    get { return wholeNumbers; }
    set
    {
      wholeNumbers = value;
      UpdateSliderContraints();
    }
  }

  [SerializeField]
  float minValue = 0;
  public float MinValue
  {
    get { return minValue; }
    set {
      minValue = value;
      UpdateSliderContraints();
    }
  }

  [SerializeField]
  float maxValue = 1;
  public float MaxValue
  {
    get { return maxValue; }
    set {
      maxValue = value;
      UpdateSliderContraints();
    }
  }

  void Start()
  {
    UpdateSliderContraints();
    minSlider.onValueChanged.AddListener(OnMinSliderValueChanged);
    maxSlider.onValueChanged.AddListener(OnMaxSliderValueChanged);
  }

  private void UpdateSliderContraints()
  {
    if ( minSlider != null )
    {
      minSlider.minValue = minValue;
      minSlider.maxValue = maxValue;
      minSlider.wholeNumbers = wholeNumbers;
    }
    
    if ( maxSlider != null )
    {
      maxSlider.maxValue = maxValue;
      maxSlider.minValue = minValue;
      maxSlider.wholeNumbers = wholeNumbers;
    }
  }

  void OnMaxSliderValueChanged(float value)
  {
    if ( value < minSlider.value )
    {
      minSlider.value = maxSlider.value;
    }
    UpdateFill();
  }

  void OnMinSliderValueChanged(float value)
  {
    if ( value > maxSlider.value )
    {
      maxSlider.value = minSlider.value;
    }
    UpdateFill();
  }

  private void OnValidate()
  {
    UpdateSliderContraints();
    UpdateFill();
  }

  private void UpdateFill()
  {
    if ( fill == null )
    {
      return;
    }

    var minAnchor = fill.anchorMin;
    minAnchor.x = (minSlider.value - minSlider.minValue) / (minSlider.maxValue - minSlider.minValue);
    fill.anchorMin = minAnchor;

    var maxAnchor = fill.anchorMax;
    maxAnchor.x = (maxSlider.value - maxSlider.minValue) / (maxSlider.maxValue - maxSlider.minValue);
    fill.anchorMax = maxAnchor;
  }

}
