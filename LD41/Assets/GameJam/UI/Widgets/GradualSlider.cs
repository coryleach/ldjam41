﻿using UnityEngine;
using UnityEngine.UI;

namespace GameJam.UI
{
  /// <summary>
  /// The purpose of this slider is to show the gradual change over time as well as the immediate change
  /// This is achieved by effectively having two sliders one for immediate and one that is lerped
  /// This effect is commonly used in health bars
  /// </summary>
  public class GradualSlider : MonoBehaviour
  {

    [SerializeField]
    [Range(0f, 1f)]
    float _value = 0;
    public float Value
    {
      get
      {
        return _value;
      }
      set
      {
        _value = Mathf.Clamp01(value);

        if (immediateSlider != null)
        {
          immediateSlider.value = _value;
        }

        if (t <= 0)
        {
          t = delay;
        }

        enabled = true;
      }
    }

    float delayValue = 0;
    public float DelayValue
    {
      get
      {
        return delayValue;
      }
      protected set
      {

        delayValue = value;

        if (delaySlider != null)
        {
          delaySlider.value = delayValue;
        }

      }
    }

    [SerializeField]
    Slider immediateSlider;

    [SerializeField]
    Slider delaySlider;

    public float delay = 0.75f;
    public float smoothTime = 0.25f;

    float velocity = 0;
    float t = 0;

    void Start()
    {

      delayValue = _value;

      if (immediateSlider != null)
      {
        immediateSlider.value = _value;
      }

      if (delaySlider != null)
      {
        delaySlider.value = delayValue;
      }

    }

    void Update()
    {

      if (Value > DelayValue)
      {
        //Instant update for inrements or 'heals'
        DelayValue = Value;
      }
      else
      {

        //Gradual drain should happen after a short delay
        if (t > 0)
        {
          t -= UnityEngine.Time.deltaTime;
          return;
        }

        //Gradual drain for decrements or 'damage'
        DelayValue = Mathf.SmoothDamp(DelayValue, Value, ref velocity, smoothTime);

      }

      if (delaySlider != null)
      {
        delaySlider.value = DelayValue;
      }

      if (Mathf.Approximately(DelayValue, Value))
      {
        DelayValue = Value;
        enabled = false;
      }

    }

    #region Testing

    [ContextMenu("Increment 0.25")]
    public void InremementQuarter()
    {
      Value += 0.25f;
    }

    [ContextMenu("Decrement 0.5")]
    public void DecrementHalf()
    {
      Value -= 0.5f;
    }

    [ContextMenu("Decrement 0.25")]
    public void DecrementQuarter()
    {
      Value -= 0.25f;
    }

    [ContextMenu("Decrement 0.1")]
    public void DecrementTenth()
    {
      Value -= 0.1f;
    }

    #endregion

  }

}
