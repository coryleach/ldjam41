﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor.UI;
using UnityEditor;

namespace GameJam.UI
{
  [CustomEditor(typeof(PagedScrollRect))]
  public class PagedScrollRectEditor : ScrollRectEditor
  {

    SerializedProperty page;

    protected override void OnEnable()
    {
      base.OnEnable();
      page = serializedObject.FindProperty("page");
    }

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      serializedObject.Update();
      EditorGUILayout.PropertyField(page);
      serializedObject.ApplyModifiedProperties();
    }

  }

}