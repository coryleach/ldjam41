﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameJam.UI
{

  public class NestedScrollRect : ScrollRect
  {

    ScrollRect parentScrollRect;

    bool sendToParent = false;

    protected override void Start()
    {
      if ( transform.parent != null )
      {
        parentScrollRect = transform.parent.GetComponentInParent<ScrollRect>();
      }
    }

    public override void OnInitializePotentialDrag(PointerEventData eventData)
    {
      sendToParent = false;

      if ( parentScrollRect != null )
      {
        parentScrollRect.OnInitializePotentialDrag(eventData);
      }

      base.OnInitializePotentialDrag(eventData);
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
      sendToParent = false;

      if (parentScrollRect == null)
      {
        sendToParent = false;
      }
      else if (!horizontal && Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
      {
        sendToParent = true;
      }
      else if (!vertical && Mathf.Abs(eventData.delta.y) > Mathf.Abs(eventData.delta.x))
      {
        sendToParent = true;
      }

      if (sendToParent)
      {
        parentScrollRect.OnBeginDrag(eventData);
      }
      else
      {
        base.OnBeginDrag(eventData);
      }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
      if ( sendToParent )
      {
        parentScrollRect.OnEndDrag(eventData);
      }
      else
      {
        base.OnEndDrag(eventData);
      }
    }

    public override void OnDrag(PointerEventData eventData)
    {
      if ( sendToParent )
      {
        parentScrollRect.OnDrag(eventData);
      }
      else
      {
        base.OnDrag(eventData);
      }
    }

  }

}