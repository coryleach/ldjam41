﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.UI
{
  public class PageSelectButton : MonoBehaviour
  {
    [SerializeField]
    int page = 0;

    [SerializeField]
    PagedScrollRect pagedScrollRect;

    [SerializeField]
    LayoutElement layoutElement;

    [SerializeField]
    float normalWidth = 1f;

    [SerializeField]
    float selectedWidth = 1.5f;

    [SerializeField]
    float elasticity = 0.1f;

    float velocity = 0;

    private void Start()
    {

    }

    public void SelectPage()
    {
      pagedScrollRect.Page = page;
    }

    private void LateUpdate()
    {
      if ( pagedScrollRect != null )
      {
        layoutElement.flexibleWidth = Mathf.SmoothDamp(layoutElement.flexibleWidth, pagedScrollRect.Page == page ? selectedWidth : normalWidth, ref velocity, elasticity);
      }
    }

  }
}