﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public class PageButtonHighlight : MonoBehaviour
  {
    [SerializeField]
    PagedScrollRect pagedScrollRect;

    [SerializeField]
    float elasticity = 0.1f;

    [SerializeField]
    RectTransform[] buttons;

    RectTransform target = null;

    RectTransform rectTransform;

    Vector2 ptVelocity = Vector2.zero;

    private void Start()
    {
      rectTransform = transform as RectTransform;
      pagedScrollRect.OnPageChanged.AddListener(PageChanged);
      rectTransform.anchorMin = new Vector2(0, 1);
      rectTransform.anchorMax = new Vector2(0, 1);
    }

    void PageChanged(int page)
    {
      target = null;
      if ( page < buttons.Length )
      {
        target = buttons[page];
      }
    }

    private void LateUpdate()
    {
      if ( target == null )
      {
        if ( pagedScrollRect != null )
        {
          PageChanged(pagedScrollRect.Page);
        }
        return;
      }

      if (target.sizeDelta.x > rectTransform.sizeDelta.x)
      {
        rectTransform.sizeDelta = target.sizeDelta;
      }

      var page1 = Mathf.FloorToInt(pagedScrollRect.PageFraction);
      var page2 = Mathf.CeilToInt(pagedScrollRect.PageFraction);

      if ( page1 < 0 )
      {
        page1 = 0;
      }

      if ( page2 >= buttons.Length )
      {
        page2 = buttons.Length - 1;
      }

      rectTransform.anchoredPosition = Vector2.Lerp(buttons[page1].anchoredPosition, buttons[page2].anchoredPosition, pagedScrollRect.PageFraction - page1);

    }

  }

}
