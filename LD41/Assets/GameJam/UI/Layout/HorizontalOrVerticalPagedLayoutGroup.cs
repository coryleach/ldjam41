﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.UI
{

  public abstract class HorizontalOrVerticalPagedLayoutGroup : LayoutGroup, ILayoutSelfController
  {

    public float spacing = 0;

    [SerializeField]
    protected RectTransform viewport;
    public RectTransform Viewport
    {
      get { return viewport; }
    }

  }

}