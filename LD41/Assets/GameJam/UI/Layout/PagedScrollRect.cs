﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameJam.UI
{
  public class PagedScrollRect : ScrollRect
  {

    [SerializeField]
    int page = 0;
    public int Page
    {
      get { return page; }
      set
      {
        if ( page != value )
        {
          page = value;
          OnPageChanged.Invoke(page);
        }
      }
    }

    public float PageFraction
    {
      get { return normalizedPosition[horizontal ? 0 : 1] * (content.childCount - 1); }
    }

    [System.Serializable]
    public class PagedScrollRectEvent : UnityEvent<int> {}

    public PagedScrollRectEvent OnPageChanged = new PagedScrollRectEvent();

    float pageVelocity = 0;
    bool dragging = false;
    int dragPage = 0;
    float dragStartPosition = 0;

    public override void OnBeginDrag(PointerEventData eventData)
    {
      base.OnBeginDrag(eventData);
      dragging = true;
      dragStartPosition = normalizedPosition[horizontal ? 0 : 1];
      dragPage = page;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
      base.OnEndDrag(eventData);

      dragging = false;

      if (content.childCount <= 1)
      {
        return;
      }

      var endPosition = normalizedPosition[horizontal ? 0 : 1];
      float pageSize = (1 / (float)(content.childCount));
      float pagePosition = dragPage / (float)(content.childCount - 1);

      if ( endPosition > dragStartPosition )
      {
        if ( (endPosition - pagePosition) > (pageSize*0.1f) )
        {
          dragPage += 1;
        }
      }
      else if ( endPosition < dragStartPosition )
      {
        if ( (pagePosition - endPosition) > (pageSize * 0.1f))
        {
          dragPage -= 1;
        }
      }

      if ( page != dragPage )
      {
        page = dragPage;
        page = Mathf.Clamp(page, 0, content.childCount - 1);
        OnPageChanged.Invoke(page);
      }

    }

    protected override void LateUpdate()
    {
      base.LateUpdate();

      int totalPages = content.childCount;

      if (totalPages <= 1)
      {
        return;
      }

      int axis = horizontal ? 0 : 1;

      if ( dragging )
      {
        int newPage = Mathf.Clamp(Mathf.RoundToInt(normalizedPosition[axis] * (totalPages-1)),0,totalPages-1);
        if ( dragPage != newPage )
        {
          dragStartPosition = normalizedPosition[axis];
          dragPage = newPage;
        }
        return;
      }

      if ( content.rect.size[axis] < viewport.rect.size[axis] )
      {
        return;
      }

      float pageSize = content.rect.width / totalPages;
      var childRect = content.GetChild(page).GetComponent<RectTransform>();

      //How much bigger our content is than our view
      var hiddenLength = content.rect.size[axis] - viewport.rect.size[axis];
      var childBounds = GetBounds(page);
      
      var targetPosition = (childBounds.min[axis] - content.rect.min[axis]) / hiddenLength;

      float currentPosition = horizontal ? horizontalNormalizedPosition : verticalNormalizedPosition;

      currentPosition = Mathf.SmoothDamp(currentPosition, targetPosition, ref pageVelocity, elasticity);

      if (horizontal)
      {
        horizontalNormalizedPosition = currentPosition;
      }
      else
      {
        verticalNormalizedPosition = currentPosition;
      }
    }

    private readonly Vector3[] corners = new Vector3[4];
    private Bounds GetBounds(int childIndex)
    {
      if (content == null)
      {
        return new Bounds();
      }

      var child = content.GetChild(childIndex) as RectTransform;

      var vMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
      var vMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);

      var toLocal = content.worldToLocalMatrix;
      child.GetWorldCorners(corners);
      for (int j = 0; j < 4; j++)
      {
        Vector3 v = toLocal.MultiplyPoint3x4(corners[j]);
        vMin = Vector3.Min(v, vMin);
        vMax = Vector3.Max(v, vMax);
      }
     
      var bounds = new Bounds(vMin, Vector3.zero);
      bounds.Encapsulate(vMax);
      return bounds;
    }

  }
}
