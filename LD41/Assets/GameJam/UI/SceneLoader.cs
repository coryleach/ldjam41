﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;

namespace GameJam.UI
{

  public class SceneLoader : MonoBehaviour
  {

    static SceneLoader current;
    public static SceneLoader Current
    {
      get
      {
        return current;
      }
    }

    void Awake()
    {

      current = this;

      if ( transitionPanel != null ) 
      {
      	transitionPanel.gameObject.SetActive(false);
      }

    }

    [SerializeField]
    PanelController transitionPanel;
    
    [System.Serializable]
    public class OnProgressUpdate : UnityEvent<float> { }

    public UnityEvent onStartSceneLoad;
    public OnProgressUpdate onProgressUpdate;
    public UnityEvent onFinishSceneLoad;

    bool isLoading = false;
    public bool IsLoading
    {
      get
      {
        return isLoading;
      }
    }

    float progress = 0;
    public float Progress
    {
      get
      {
        return progress;
      }
    }

    public void LoadScene( string sceneName, Action callback = null)
    {

      StartLoad();

      if ( transitionPanel != null )
      {
          transitionPanel.Show( PanelOptions.Animate, () => { InternalLoadScene( sceneName, callback ); } );
      }
      else
      {
        InternalLoadScene( sceneName, callback );
      }

    }

    void InternalLoadScene(string sceneName, Action callback)
    {
      StartCoroutine( LoadSceneCoroutine(sceneName, callback) );
    }

    IEnumerator LoadSceneCoroutine( string sceneName, Action callback )
    {
   	
      var loader = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( sceneName );

      while ( !loader.isDone )
      {
        progress = loader.progress;
        onProgressUpdate.Invoke(progress);
        yield return null;
      }

      //Progress Complete
      progress = 1f;
      onProgressUpdate.Invoke(progress);

      //Hide Transition Panel
      if ( transitionPanel != null )
      {
        transitionPanel.Hide( PanelOptions.Animate, () => {
          FinishLoad(callback);
        } );
      }
      else
      {
        FinishLoad(callback);
      }
    }

    void StartLoad()
    {
      UIEventManager.LockCurrent();
      isLoading = true;
      progress = 0;
      onStartSceneLoad.Invoke();
    }

    void FinishLoad(Action callback = null)
    {
      isLoading = false;
      onFinishSceneLoad.Invoke();
      if ( callback != null )
      {
        callback();
      }
      UIEventManager.UnlockCurrent();
    }

  }

}
