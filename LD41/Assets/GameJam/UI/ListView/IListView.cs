﻿using UnityEngine;
using System.Collections.Generic;

namespace GameJam.UI
{
  /// <summary>
  /// Interface for a view that displays a list
  /// </summary>
  /// <typeparam name="TDataItem">List view will display a list of views associated with this type</typeparam>
  public interface IListView<TDataItem>
  {
    /// <summary>
    /// List of items. ListViewItem will be generated for each item in the list.
    /// </summary>
    IList<TDataItem> List { get; set; }
  }
}
