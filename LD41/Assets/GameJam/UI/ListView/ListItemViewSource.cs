﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GameJam.UI
{

  public class ListItemViewSource : MonoBehaviour
  {

    public RectTransform prefab;

    [System.Serializable]
    public class FooDataModel
    {
      public string data;
      public int number;
    }

    [SerializeField]
    List<FooDataModel> list = new List<FooDataModel>();

    public List<FooDataModel> ItemSource
    {
      get { return list; }
      set
      {
        list = value;
      }
    }

    public int ItemCount
    {
      get
      {
        return list.Count;
      }
    }

    public float GetPrefabWidth(int index)
    {
      return prefab.rect.width;
    }

    public float GetPrefabHeight(int index)
    {
      return prefab.rect.height;
    }

    public void CreateOrUpdateItemViewForContainer(ScrollListItemViewContainer viewContainer, Transform parent)
    {
      if (viewContainer.transform == null)
      {
        viewContainer.transform = Instantiate(prefab, parent);
      }

      var text = viewContainer.transform.GetComponentInChildren<Text>();
      if (text)
      {
        text.text = string.Format("{2} - {0}.{1}", list[viewContainer.index].data, list[viewContainer.index].number, viewContainer.index);
      }
    }

    public void ReturnItemView(ScrollListItemViewContainer viewContainer)
    {
      Destroy(viewContainer.transform.gameObject);
    }

    private void OnEnable()
    {
      StartCoroutine(AddItems());
    }

    IEnumerator AddItems()
    {
      while (true)
      {
        yield return new WaitForSeconds(Random.Range(2f, 5f));
        var foo = new FooDataModel();
        foo.data = "Chat chat";
        foo.number = list.Count;
        list.Add(foo);
        OnSourceChanged.Invoke();
      }
    }

    public UnityEvent OnSourceChanged = new UnityEvent();

    private void OnValidate()
    {
      if (Application.isPlaying)
      {
        OnSourceChanged.Invoke();
      }
    }

  }

}
