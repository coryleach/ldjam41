﻿using UnityEngine;
using System.Collections;

namespace GameJam.UI
{
  /// <summary>
  /// View item of a list. This would be one widget entry in a list.
  /// </summary>
  /// <typeparam name="TDataItem">Data Item</typeparam>
  public interface IListViewItem<TDataItem>
  {
    TDataItem Item { get; set; }
  }
}
