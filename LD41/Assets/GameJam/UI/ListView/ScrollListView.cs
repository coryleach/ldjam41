﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using GameJam.Extensions;
using System;

namespace GameJam.UI
{

  [System.Serializable]
  public class ScrollListItemViewContainer
  {
    public RectTransform transform;
    public int index;
  }

  [RequireComponent(typeof(ScrollRect))]
  public class ScrollListView : UIBehaviour, IInitializePotentialDragHandler
  {
    ScrollRect scrollRect = null;
    public ScrollRect ScrollRect
    {
      get
      {
        if ( scrollRect == null ) { scrollRect = GetComponent<ScrollRect>(); }
        return scrollRect;
      }
    }

    RectTransform content = null;
    public RectTransform Content
    {
      get
      {
        if ( content == null ) { content = ScrollRect.content; }
        return content;
      }
    }

    RectTransform viewport = null;
    public RectTransform Viewport
    {
      get
      {
        if ( viewport == null ) { viewport = ScrollRect.viewport; }
        return viewport;
      }
    }

    [SerializeField]
    ListItemViewSource itemSource;
    public ListItemViewSource ItemSource
    {
      get { return itemSource; }
      set
      {
        if ( itemSource != null ) { itemSource.OnSourceChanged.RemoveListener(OnListSourceChanged); }
        SetProperty(ref itemSource, value);
        itemSource.OnSourceChanged.AddListener(OnListSourceChanged);
      }
    }

    [SerializeField]
    RectOffset padding = new RectOffset();
    public RectOffset Padding {  get { return padding; } set { SetProperty(ref padding, value); } }

    [SerializeField]
    protected float spacing = 0;
    public float Spacing { get { return spacing; } set { SetProperty(ref spacing, value); } }

    public bool IsHorizontal
    {
      get { return ScrollRect.horizontal; }
    }

    public bool IsVertical
    {
      get { return ScrollRect.vertical; }
    }

    private RectTransform.Edge StartEdge
    {
      get
      {
        if (IsHorizontal)
        {
          return RectTransform.Edge.Left;
        }
        return RectTransform.Edge.Top;
      }
    }

    private RectTransform.Edge EndEdge
    {
      get
      {
        if (IsHorizontal)
        {
          return RectTransform.Edge.Right;
        }
        return RectTransform.Edge.Bottom;
      }
    }

    public Vector2 defaultScrollPosition = new Vector2(0, 1);

    private List<ScrollListItemViewContainer> visibleItemViews = new List<ScrollListItemViewContainer>();
    private List<ScrollListItemViewContainer> reusableContainers = new List<ScrollListItemViewContainer>();

    private Vector2 previousScrollPosition = Vector2.one;

    private float[] sizes;
    private float[] cumulativeSizes;

    #region UIBehaviour

    protected override void Start()
    {

      if (IsHorizontal && IsVertical)
      {
        Debug.LogError("Both Horizontal and Vertical scrolling is not supported");
      }

      ScrollRect.onValueChanged.AddListener(OnScroll);

      if ( itemSource != null )
      {
        itemSource.OnSourceChanged.AddListener(OnListSourceChanged);
        InitializeList();
      }

    }

    protected override void OnRectTransformDimensionsChange()
    {
      base.OnRectTransformDimensionsChange();
    }

    protected virtual void LateUpdate()
    {
      if ( smoothMoveTo )
      {
        ScrollRect.normalizedPosition = Vector2.SmoothDamp(ScrollRect.normalizedPosition, targetScrollPosition, ref velocity, ScrollRect.elasticity, Mathf.Infinity, UnityEngine.Time.deltaTime);
      }
    }

    #endregion

    #region Input

    bool smoothMoveTo = false;
    Vector2 velocity = Vector2.zero;
    Vector2 targetScrollPosition = Vector2.zero;

    public void OnInitializePotentialDrag(PointerEventData eventData)
    {
      smoothMoveTo = false;
    }

    #endregion

    #region Callbacks

    protected void OnScroll(Vector2 position)
    {
      ComputeVisibility(position);
    }

    protected void OnListSourceChanged()
    {
      CollectAllViewStartSizes();

      //Check if we should scroll to previous scroll position after updating the layout
      //Generally we only want to do this if we're scrolled all the way to one of the ends of the list
      bool scrollToPrevious = false;
      if (IsHorizontal && ( previousScrollPosition.x <= 0 || previousScrollPosition.x >= 1 ))
      {
        scrollToPrevious = true;
      }
      else if (IsVertical && ( previousScrollPosition.y <= 0 || previousScrollPosition.y >= 1 ))
      {
        scrollToPrevious = true;
      }

      if (scrollToPrevious)
      {
        smoothMoveTo = true;
        targetScrollPosition = previousScrollPosition;
      }

      float cumulativeSize = CalculateCumulativeSizes();
      SetContentSize(cumulativeSize);

      ComputeVisibility(previousScrollPosition);

      ScrollRect.Rebuild(CanvasUpdate.PostLayout);
      Canvas.ForceUpdateCanvases();
    }

    #endregion

    private void InitializeList()
    {
      ClearViews();

      //Initialize a new list
      CollectAllViewStartSizes();

      //Update content size
      float cumulativeSize = CalculateCumulativeSizes();
      SetContentSize(cumulativeSize);

      ScrollToDefault();

      ComputeVisibility(ScrollRect.normalizedPosition);

    }

    protected float GetInsetForIndex(int index)
    {
      float inset = GetStartPadding();

      if (index > 0)
      {
        inset += cumulativeSizes[index - 1] + (index * spacing);
      }

      return inset;
    }

    protected float GetViewportSize()
    {
      if ( IsHorizontal )
      {
        return Viewport.rect.width;
      }
      else
      {
        return Viewport.rect.height;
      }
    }

    protected void ClearViews()
    {
      for ( int i = 0; i < visibleItemViews.Count; i++ )
      {
        ItemSource.ReturnItemView(visibleItemViews[i]);
      }
      visibleItemViews.Clear();

      for (int i = 0; i < reusableContainers.Count; i++)
      {
        ItemSource.ReturnItemView(reusableContainers[i]);
      }
      reusableContainers.Clear();
    }

    protected float GetStartPadding()
    {
      if (IsHorizontal)
      {
        return padding.left;
      }
      else
      {
        return padding.top;
      }
    }

    protected float GetEndPadding()
    {
      if ( IsHorizontal )
      {
        return padding.right;
      }
      else
      {
        return padding.bottom;
      }
    }

    protected float GetContentSize()
    {
      if ( IsHorizontal )
      {
        return Content.rect.width;
      }
      else
      {
        return Content.rect.height;
      }
    }

    protected void ScrollToStart()
    {
      if (IsHorizontal)
      {
        ScrollRect.horizontalNormalizedPosition = 0f;
      }
      else
      {
        ScrollRect.verticalNormalizedPosition = 1f;
      }
    }

    protected void ScrollToEnd()
    {
      if (IsHorizontal)
      {
        ScrollRect.horizontalNormalizedPosition = 1f;
      }
      else
      {
        ScrollRect.verticalNormalizedPosition = 0f;
      }
    }

    protected void ScrollToDefault()
    {
      if (IsHorizontal)
      {
        ScrollRect.horizontalNormalizedPosition = defaultScrollPosition.x;
      }
      else
      {
        ScrollRect.verticalNormalizedPosition = defaultScrollPosition.y;
      }
    }

    protected void CollectAllViewStartSizes()
    {
      AssureItemSizesArrayCapacity();

      //Init all sizes to prefab size
      if (IsHorizontal)
      {
        for (int i = 0; i < ItemSource.ItemCount; ++i)
        {
          sizes[i] = ItemSource.GetPrefabWidth(i);
        }
      }
      else
      {
        for (int i = 0; i < ItemSource.ItemCount; ++i)
        {
          sizes[i] = ItemSource.GetPrefabHeight(i);
        }
      }
    }

    protected void AssureItemSizesArrayCapacity()
    {
      // Create array of sizes
      if (sizes == null || sizes.Length != ItemSource.ItemCount)
      {
        sizes = new float[ItemSource.ItemCount];
      }

      if (cumulativeSizes == null || cumulativeSizes.Length != ItemSource.ItemCount)
      {
        cumulativeSizes = new float[ItemSource.ItemCount];
      }
    }

    protected float CalculateCumulativeSizes()
    {
      AssureItemSizesArrayCapacity();
      float cumulativeSize = 0f;

      //int realIndex;
      for (int index = 0; index < ItemSource.ItemCount; ++index)
      {
        cumulativeSize += sizes[index];
        cumulativeSizes[index] = cumulativeSize;
      }

      return cumulativeSize;
    }

    protected void SetContentSize(float cumulativeItemSize)
    {
      float contentPanelSize = cumulativeItemSize;

      if ( IsHorizontal )
      {
        contentPanelSize += padding.left + padding.right;
      }
      else
      {
        contentPanelSize += padding.top + padding.bottom;
      }

      if (ItemSource.ItemCount > 0)
      {
        contentPanelSize += spacing * (ItemSource.ItemCount - 1);
      }

      float inset = Content.GetInsetFromParentEdge(Viewport, StartEdge);

      //Content shouldn't move. Only grow away from the start edge
      Content.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(Viewport, StartEdge, inset, contentPanelSize);

      ScrollRect.Rebuild(CanvasUpdate.PostLayout);
      Canvas.ForceUpdateCanvases();
    }

    protected void ComputeVisibility(Vector2 position)
    {
      
      int indexDirection = 0;
      if (IsHorizontal)
      {
        indexDirection = (position.x - previousScrollPosition.x) >= 0 ? 1 : -1;
      }
      else
      {
        indexDirection = (position.y - previousScrollPosition.y) > 0 ? -1 : 1;
      }

      //Debug.LogFormat("Position {0} Prev {1} Direction {2}",position,previousScrollPosition,indexDirection);

      previousScrollPosition = position;

      Vector3[] corners = new Vector3[4];
      Viewport.GetWorldCorners(corners);

      for (int i = 0; i < corners.Length; i++)
      {
        corners[i] = Content.InverseTransformPoint(corners[i]);
        corners[i].x += Content.pivot.x * Content.rect.width;
        corners[i].y -= (1 - Content.pivot.y) * Content.rect.height;
      }

      float startPos = (IsHorizontal ? corners[0].x : (corners[1].y * -1));
      float endPos = startPos + (IsHorizontal ? (corners[2].x - corners[0].x) : (corners[1].y - corners[0].y));

      int currentStartIndex = 0;
      int currentEndIndex = 0;

      if (visibleItemViews.Count > 0)
      {
        currentStartIndex = visibleItemViews[0].index;
        currentEndIndex = visibleItemViews[visibleItemViews.Count - 1].index;
      }

      if (indexDirection > 0)
      {
        //Scrolling Down
        //Increment current start index until we have a start index that will be at least partially in view
        while (currentStartIndex < (ItemSource.ItemCount - 1) && (GetInsetForIndex(currentStartIndex) + sizes[currentStartIndex]) < startPos)
        {
          currentStartIndex += 1;
        }

        while (currentEndIndex < (ItemSource.ItemCount - 1) && (GetInsetForIndex(currentEndIndex) + sizes[currentEndIndex]) < endPos)
        {
          currentEndIndex += 1;
        }

      }
      else
      {
        //Scrolling Up
        //If start of current index is below the view start position then we need to move the index backwards
        while (currentStartIndex > 0 && GetInsetForIndex(currentStartIndex) > startPos)
        {
          currentStartIndex -= 1;
        }

        while (currentEndIndex > 0 && GetInsetForIndex(currentEndIndex) > endPos)
        {
          currentEndIndex -= 1;
        }

      }

      //Remove containers no longer in view
      for (int i = (visibleItemViews.Count - 1); i >= 0; i--)
      {
        if (visibleItemViews[i].index > currentEndIndex || visibleItemViews[i].index < currentStartIndex)
        {
          //Remove
          reusableContainers.Add(visibleItemViews[i]);
          visibleItemViews.RemoveAt(i);
        }
      }

      int visibleItemIndex = 0;
      for (int index = currentStartIndex; index <= currentEndIndex && index < ItemSource.ItemCount; index++)
      {
        ScrollListItemViewContainer container = null;

        if (visibleItemViews.Count != 0)
        {
          if (visibleItemIndex >= visibleItemViews.Count)
          {
            container = GetOrCreateContainer();
            visibleItemViews.Add(container);
          }
          else if (index < visibleItemViews[visibleItemIndex].index)
          {
            //Create a new one and insert it before current visible item
            container = GetOrCreateContainer();
            visibleItemViews.Insert(visibleItemIndex, container);
          }
          else if (index > visibleItemViews[visibleItemIndex].index)
          {
            //Index of item is greater than current start item
            container = GetOrCreateContainer();
            visibleItemViews.Insert(visibleItemIndex, container);
          }
          else
          {
            //Equal
            container = visibleItemViews[visibleItemIndex];
          }
        }
        else
        {
          container = GetOrCreateContainer();
          visibleItemViews.Add(container);
        }

        visibleItemIndex += 1;

        container.index = index;

        ItemSource.CreateOrUpdateItemViewForContainer(container, this.content);

        //Ensure container transform is active
        container.transform.gameObject.SetActive(true);

        //Position transform within rect
        float inset = GetStartPadding();

        if (index > 0)
        {
          inset += cumulativeSizes[index - 1] + (index * spacing);
        }

        container.transform.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(Content, StartEdge, inset, sizes[index]);
        container.transform.name = string.Format("[{0}]", index);

      }

      //Disable remaining reusable containers
      for (int i = 0; i < reusableContainers.Count; i++)
      {
        if (reusableContainers[i].transform != null)
        {
          reusableContainers[i].transform.gameObject.SetActive(false);
        }
      }
    }

    protected ScrollListItemViewContainer GetOrCreateContainer()
    {
      ScrollListItemViewContainer container = null;
      //Create a new one and insert it before current visible item
      if (reusableContainers.Count > 0)
      {
        container = reusableContainers[reusableContainers.Count - 1];
        reusableContainers.RemoveAt(reusableContainers.Count - 1);
      }
      else
      {
        container = new ScrollListItemViewContainer();
      }
      return container;
    }

    protected void SetProperty<T>(ref T currentValue, T newValue)
    {
      if ((currentValue == null && newValue == null) || (currentValue != null && currentValue.Equals(newValue)))
      {
        return;
      }
      currentValue = newValue;
    }

  }

}