﻿using UnityEngine;
using System.Collections.Generic;

namespace GameJam.UI
{
  /// <summary>
  /// Abstract implementation of a view of a list
  /// </summary>
  /// <typeparam name="TDataType">Data time of items in the list</typeparam>
  public abstract class ListView<TDataType> : MonoBehaviour, IListView<TDataType>
  {
    public abstract IList<TDataType> List { get; set; }
    protected abstract ListViewItem<TDataType> CreateNewView();
    protected List<ListViewItem<TDataType>> viewList = new List<ListViewItem<TDataType>>();

    #region Monobehaviour

    protected virtual void OnEnable()
    {
      RefreshList();
    }

    #endregion

    protected virtual void DisableView(ListViewItem<TDataType> view)
    {
      view.gameObject.SetActive(false);
    }

    protected virtual void EnableView(ListViewItem<TDataType> view)
    {
      view.gameObject.SetActive(true);
    }

    protected virtual void RefreshList()
    {
      if (List == null)
      {
        return;
      }

      int i = 0;
      for (; i < List.Count; i++)
      {
        var item = List[i];

        ListViewItem<TDataType> view = null;
        if (i < viewList.Count)
        {
          //Update existing item
          view = viewList[i];
        }
        else
        {
          //Create new view
          view = CreateNewView();
          viewList.Add(view);
        }

        //Update the item value
        EnableView(view);
        view.Item = item;
      }

      //Clean up any items no longer being used
      for (; i < viewList.Count; i++)
      {
        DisableView(viewList[i]);
      }
    }
  }
}