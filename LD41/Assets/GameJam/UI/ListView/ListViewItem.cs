﻿using UnityEngine;
using System.Collections;
using System;

namespace GameJam.UI
{
  /// <summary>
  /// This class represents one child view of a list view and is associated with one specific item in a list.
  /// </summary>
  /// <typeparam name="TDataItem">Type of data contained in the associated list</typeparam>
  public abstract class ListViewItem<TDataItem> : MonoBehaviour, IListViewItem<TDataItem>
  {
    public abstract TDataItem Item { get; set; }
  }
}
