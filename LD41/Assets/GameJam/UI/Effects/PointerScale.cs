﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace GameJam.UI
{
  using GameJam.Tween;
  using GameJam.Extensions;

  /// <summary>
  /// Use this behaviour to scale objects when hovered or tapped
  /// </summary>
  public class PointerScale : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
  {

    bool _isHovering = false;
    bool _isPressed = false;

    public Vector3 hover = Vector3.one;

    public Vector3 press = Vector3.one;

    public float duration = 0.1f;

    public EaseType easing = EaseType.SinInOut;

    Vector3 normal = Vector3.one;

    TweenScale _tweenScale;
    public TweenScale TweenScale
    {
      get
      {
        if (_tweenScale == null)
        {
          _tweenScale = this.gameObject.GetOrAddComponent<TweenScale>();
        }
        return _tweenScale;
      }
    }

    void Start()
    {
      normal = this.transform.localScale;
      this.TweenScale.enabled = false;
    }

    // Update is called once per frame
    public void OnPointerEnter(PointerEventData eventData)
    {
      _isHovering = true;
      Refresh();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
      _isHovering = false;
      Refresh();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      _isPressed = true;
      Refresh();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
      _isPressed = false;
      Refresh();
    }

    void Refresh()
    {
      var targetScale = this.normal;

      if (_isHovering)
      {
        if (_isPressed)
        {
          targetScale = this.press;
        }
        else
        {
          targetScale = this.hover;
        }
      }

      if (this.transform.localScale != targetScale)
      {
        //Tween
        TweenScale.start = this.transform.localScale;
        TweenScale.end = targetScale;
        TweenScale.duration = this.duration;
        TweenScale.delay = 0;
        TweenScale.easing = this.easing;
        TweenScale.Play();
      }

    }

  }

}