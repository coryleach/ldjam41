﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Events;

namespace GameJam.UI
{

  [CreateAssetMenu(menuName ="GameJam/UI/MenuSystems/Stack")]
  public class PanelStackSystem : PanelMenuSystem
  {

    Stack<IPanelOptions> panelStack = new Stack<IPanelOptions>();

    [SerializeField]
    PanelType defaultPanel = null;
    public PanelType DefaultPanel { get { return defaultPanel; } }

    public IPanelOptions CurrentPanelOptions
    {
      get
      {
        if ( panelStack.Count > 0 )
        {
          return panelStack.Peek();
        }
        return null;
      }
    }

    public virtual void Push(IPanelOptions options)
    {
      var previousOptions = CurrentPanelOptions;
      panelStack.Push(options);
      var newOptions = CurrentPanelOptions;
      onPanelTransition.Raise(previousOptions, newOptions, PanelTransitionDirection.Forward);
    }

    public virtual void Pop()
    {
      if ( panelStack.Count == 0 )
      {
        return;
      }

      var previousOptions = CurrentPanelOptions;
      panelStack.Pop();
      var newOptions = CurrentPanelOptions;
      onPanelTransition.Raise(previousOptions, newOptions, PanelTransitionDirection.Backward);
    }

    public virtual void Clear()
    {
      if (panelStack.Count > 0)
      {
        var previousOptions = CurrentPanelOptions;
        panelStack.Clear();
        onPanelTransition.Raise(previousOptions, null, PanelTransitionDirection.Backward);
      }
    }

  }

}