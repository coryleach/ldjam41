﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  [CreateAssetMenu(menuName="GameJam/UI/MenuSystems/Tabbed")]
  public class PanelTabSystem : PanelMenuSystem
  {

    [SerializeField]
    PanelType defaultTab;
    public PanelType DefaultTab { get { return defaultTab; } }

    IPanelOptions currentOptions;
    public IPanelOptions CurrentPanelOptions { get { return currentOptions; } }

    public void ShowTab(IPanelOptions options)
    {
      var oldOptions = currentOptions;
      currentOptions = options;
      onPanelTransition.Raise(oldOptions, currentOptions, PanelTransitionDirection.Forward);
    }
    
  }

}