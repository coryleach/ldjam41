﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  [CreateAssetMenu(menuName = "GameJam/UI/MenuSystems/Single")]
  public class PanelSingleSystem : PanelMenuSystem
  {
    [SerializeField]
    PanelType defaultPanel;
    public PanelType DefaultPanel { get { return defaultPanel; } }

    IPanelOptions currentOptions;
    public IPanelOptions CurrentPanelOptions { get { return currentOptions; } }

    public void Show(IPanelOptions options)
    {
      var oldOptions = currentOptions;
      currentOptions = options;
      onPanelTransition.Raise(oldOptions, currentOptions, PanelTransitionDirection.Forward);
    }
  }

}