﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public abstract class PanelMenuSystem : ScriptableObject
  {
    public PanelTransitionEvent onPanelTransition;
  }

}