﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Events;

namespace GameJam.UI
{

  [CreateAssetMenu(menuName = "GameJam/UI/MenuSystems/Queue")]
  public class PanelQueueSystem : PanelMenuSystem
  {

    public GameEvent onPanelEnqueued;

    Queue<IPanelOptions> panelQueue = new Queue<IPanelOptions>();

    IPanelOptions currentOptions = null;
    public IPanelOptions CurrentOptions { get { return currentOptions; } }

    public void EnqueuePanel(IPanelOptions options)
    {
      panelQueue.Enqueue(options);
      onPanelEnqueued.Raise();
    }

    public void DismissPanel()
    {
      if (currentOptions != null)
      {
        var oldOptions = currentOptions;
        currentOptions = null;
        onPanelTransition.Raise(oldOptions, null, PanelTransitionDirection.Forward);
      }
    }

    public void DequeuePanel()
    {
      if (panelQueue.Count == 0 || currentOptions != null)
      {
        return;
      }
      currentOptions = panelQueue.Dequeue();
      onPanelTransition.Raise(null, currentOptions, PanelTransitionDirection.Forward);
    }

  }

}