﻿using UnityEngine;
using System.Collections;

namespace GameJam.UI
{

	public interface IPanelProvider 
	{

		PanelController GetPanel(IPanelOptions options);
    
	}

}
