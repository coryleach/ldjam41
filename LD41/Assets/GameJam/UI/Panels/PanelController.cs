﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace GameJam.UI
{
  /// <summary>
  /// Controller for the showing and hiding of a UI panel.
  /// </summary>
  public class PanelController : MonoBehaviour
  {

    IPanelTransition _transition;

    public IPanelTransition Transitioner
    {
      get
      {
        if ( _transition == null )
        {
          _transition = GetComponent<IPanelTransition>();
        }
        return _transition;
      }
    }

    IPanelOptions _panelOptions;

    public IPanelOptions CurrentOptions
    {
      get
      {
        return _panelOptions;
      }
      set
      {
        _panelOptions = value;
      }
    }

    IPanelAttributes _panelAttributes;
    public IPanelAttributes Attributes
    {
      get
      {
        if ( _panelAttributes == null )
        {
          _panelAttributes = GetComponent<IPanelAttributes>();
        }
        return _panelAttributes;
      }
    }

    protected bool _isShowing = false;

    public bool IsShowing
    {
      get
      {
        return _isShowing;
      }
    }

    protected bool _isTransitioning = false;
    public bool IsTransitioning
    {
      get
      {
        return _isTransitioning;
      }
    }

    public void Toggle()
    {
      if ( IsShowing )
      {
        Hide();
      }
      else
      {
        Show();
      }
    }

    public PanelEvent OnPanelShow = new PanelEvent();
    public PanelEvent OnPanelHide = new PanelEvent();

    //If we can make Show and Hide only callable by PanelStack.cs we should probably do so...
    public virtual void Show(IPanelOptions options = null, Action onDidShow = null)
    {
      if ( !_isShowing )
      {
        _isShowing = true;
        gameObject.SetActive(true);

        bool animate = (options != null) ? options.AnimateShow : false;

        if ( animate && Transitioner != null )
        {
          _isTransitioning = true;
          
          Transitioner.TransitionShow(() =>
          {

            _isTransitioning = false;

            if ( onDidShow != null )
            {
              onDidShow();
            }
            
            OnPanelShow.Invoke(this);

          });
        }
        else
        {

          if ( onDidShow != null )
          {
            onDidShow();
          }

          OnPanelShow.Invoke(this);

        }
      }
    }

    public virtual void Hide(IPanelOptions options = null, Action onDidHide = null)
    {
      if ( _isShowing )
      {
        _isShowing = false;

        bool animate = (options != null) ? options.AnimateHide : false;

        if ( animate && Transitioner != null )
        {
          _isTransitioning = true;

          Transitioner.TransitionHide(() =>
          {

            gameObject.SetActive(false);
            _isTransitioning = false;

            if ( onDidHide != null )
            {
              onDidHide();
            }

            OnPanelHide.Invoke(this);

          });
        }
        else
        {

          gameObject.SetActive(false);

          if ( onDidHide != null )
          {
            onDidHide();
          }
          
          OnPanelHide.Invoke(this);

        }
      }
    }

    public virtual void WillShow(IPanelOptions options = null)
    {
    }

    public virtual void DidShow(IPanelOptions options = null)
    {
    }

    public virtual void WillHide(IPanelOptions options = null)
    {
    }

    public virtual void DidHide(IPanelOptions options = null)
    {
    }

    /// <summary>
    /// Will pop the panel if part of a stack.
    /// Will close the panel if part of a queue.
    /// </summary>
    public void DismissSelf()
    {
      if ( CurrentOptions.MenuController != null )
      {
        var stack = CurrentOptions.MenuController as IPanelStack;
        if ( stack != null )
        {
          stack.PopPanel(CurrentOptions);
        }
        else
        {
          Debug.LogError(name + " cannot be dismissed. Menu controller was not a stack.");
        }
      }
      else
      {
        Hide();
      }
    }

  }

}