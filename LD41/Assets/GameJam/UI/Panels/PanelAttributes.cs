﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  [RequireComponent(typeof(PanelController))]
  public class PanelAttributes : MonoBehaviour, IPanelAttributes
  {

    [SerializeField]
    protected PanelType panelType;
    public PanelType PanelType
    {
      get { return panelType; }
    }

  }

}
