﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace GameJam.UI
{

  public abstract class PanelMenuController : PanelController, IPanelTransitionEventListener
  {

    protected abstract PanelMenuSystem PanelMenuSystem { get; }

    [SerializeField]
    protected PanelProvider panelProvider;
    public PanelProvider PanelProvider
    {
      get { return panelProvider; }
    }

    private bool subscribedToTransitionEvents = false;

    protected virtual void Awake()
    {
      SubscribeToSystemTransition();
    }

    protected void SubscribeToSystemTransition()
    {
      try
      {
        if ( !subscribedToTransitionEvents )
        {
          subscribedToTransitionEvents = true;
          PanelMenuSystem.onPanelTransition.AddListener(this);
        }
      }
      catch (Exception exception)
      {
        Debug.LogError("Failed to subscribe to panel system transitions. Check that controller has a panel system and the panel system has a transition event.", this);
        Debug.LogException(exception, this);
      }
    }

    /// <summary>
    /// Show a panel with the given options
    /// </summary>
    /// <param name="showPanel"></param>
    /// <param name="options"></param>
    /// <param name="callback"></param>
    protected virtual void ShowPanel(PanelController showPanel, IPanelOptions options, Action callback = null)
    {
      if (showPanel == null)
      {
        if (callback != null)
        {
          callback.Invoke();
        }
      }
      else
      {
        if ( showPanel.IsTransitioning )
        {
          Debug.LogWarning("Panel is mid transition!");
        }

        showPanel.WillShow(options);
        showPanel.Show(options, () =>
        {
          showPanel.DidShow(options);

          if (callback != null)
          {
            callback.Invoke();
          }

        });
      }
    }

    /// <summary>
    /// Hide a panel using the given options
    /// </summary>
    /// <param name="hidePanel"></param>
    /// <param name="options"></param>
    /// <param name="callback"></param>
    protected virtual void HidePanel(PanelController hidePanel, IPanelOptions options, Action callback = null)
    {
      if (hidePanel == null)
      {

        if (callback != null)
        {
          callback.Invoke();
        }

      }
      else
      {

        hidePanel.WillHide(options);
        hidePanel.Hide(options, () =>
        {
          hidePanel.DidHide(options);

          if (callback != null)
          {
            callback.Invoke();
          }
        });

      }
    }

    /// <summary>
    /// Transition between two panels using the given options
    /// </summary>
    /// <param name="hidePanel"></param>
    /// <param name="showPanel"></param>
    /// <param name="options"></param>
    protected virtual void TransitionPanel(PanelController hidePanel, PanelController showPanel, IPanelOptions hideOptions, IPanelOptions showOptions, PanelTransitionDirection direction)
    {
      PanelTransitionType transition = PanelTransitionType.ShowThenHide;

      if( direction == PanelTransitionDirection.Forward && showOptions != null )
      {
        transition = showOptions.Transition;
      }
      else if ( direction == PanelTransitionDirection.Backward && hideOptions != null )
      {
        transition = hideOptions.Transition;
      }

      switch (transition)
      {
        case PanelTransitionType.ShowThenHide:
          ShowThenHide(hidePanel, showPanel, hideOptions, showOptions);
          break;
        case PanelTransitionType.HideThenShow:
          HideThenShow(hidePanel, showPanel, hideOptions, showOptions);
          break;
        case PanelTransitionType.ShowOnly:
          ShowOnly(hidePanel, showPanel, showOptions);
          break;
        default:
          throw new Exception("Transition type not implemented");
      }
    }

    /// <summary>
    /// Shows the showPanel and after that hides the hide panel
    /// </summary>
    /// <param name="hidePanel"></param>
    /// <param name="showPanel"></param>
    /// <param name="options"></param>
    void ShowThenHide(PanelController hidePanel, PanelController showPanel, IPanelOptions hideOptions, IPanelOptions showOptions)
    {
      ShowPanel(showPanel, showOptions, () =>
      {
        HidePanel(hidePanel, hideOptions);
      });
    }

    /// <summary>
    /// Hides the hide panel and after that shows the show panel
    /// </summary>
    /// <param name="hidePanel"></param>
    /// <param name="showPanel"></param>
    /// <param name="options"></param>
    void HideThenShow(PanelController hidePanel, PanelController showPanel, IPanelOptions hideOptions, IPanelOptions showOptions)
    {
      HidePanel(hidePanel, hideOptions, () =>
      {
        ShowPanel(showPanel, showOptions);
      });
    }

    /// <summary>
    /// Will only show the showPanel and does not alter the hidePanel
    /// </summary>
    /// <param name="hidePanel"></param>
    /// <param name="showPanel"></param>
    /// <param name="options"></param>
    void ShowOnly(PanelController hidePanel, PanelController showPanel, IPanelOptions showOptions)
    {
      ShowPanel(showPanel, showOptions);
    }

    public void OnPanelTransition(IPanelOptions hideOptions, IPanelOptions showOptions, PanelTransitionDirection direction)
    {
      
      PanelController hidePanel = null;
      if (hideOptions != null)
      {
        hidePanel = panelProvider.GetPanel(hideOptions);
      }

      PanelController showPanel = null;
      if (showOptions != null)
      {
        showPanel = panelProvider.GetPanel(showOptions);
      }

      TransitionPanel(hidePanel,showPanel,hideOptions,showOptions,direction);

    }
  }

}
