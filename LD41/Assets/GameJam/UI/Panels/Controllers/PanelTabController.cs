﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public class PanelTabController : PanelMenuController
  {

    [SerializeField]
    PanelTabSystem panelTabSystem;
    protected override PanelMenuSystem PanelMenuSystem
    {
      get { return panelTabSystem; }
    }

    [SerializeField]
    PanelOptions defaultPanelOptions;

    [SerializeField]
    [Tooltip("Shows the default panel options every time this panel is shown")]
    bool showDefaultEveryTime = false;

    PanelController currentPanel = null;
    public PanelController CurrentPanel
    {
      get
      {
        return currentPanel;
      }
    }

    IPanelOptions currentPanelOptions = null;
    public IPanelOptions CurrentPanelOptions
    {
      get
      {
        return currentPanelOptions;
      }
    }

    void Start()
    {
      if ( !showDefaultEveryTime )
      {
        panelTabSystem.ShowTab(defaultPanelOptions);
      }
    }

    public override void WillShow(IPanelOptions options = null)
    {
      base.WillShow(options);
      if ( showDefaultEveryTime )
      {
        SubscribeToSystemTransition();
        panelTabSystem.ShowTab(defaultPanelOptions);
      }
    }

  }

}