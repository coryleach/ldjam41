﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace GameJam.UI
{
  /// <summary>
  /// This class implements a stack-based UI panel system
  /// </summary>
  public class PanelStackController : PanelMenuController
  {

    [SerializeField]
    PanelStackSystem panelStackSystem;
    protected override PanelMenuSystem PanelMenuSystem
    {
      get { return panelStackSystem; }
    }

    public bool clearOnEnable = false;

    protected override void Awake()
    {
      base.Awake();
      if ( !clearOnEnable )
      {
        Init();
      }
    }

    private void OnEnable()
    {
      if ( clearOnEnable )
      {
        Init();
      }
    }

    void Init()
    {
      panelStackSystem.Clear();

      var options = new PanelOptions();
      options.AnimateHide = false;
      options.AnimateShow = false;
      options.Transition = PanelTransitionType.ShowOnly;
      options.PanelType = panelStackSystem.DefaultPanel;
      panelStackSystem.Push(options);
    }

  }
}


