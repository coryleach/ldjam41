﻿using UnityEngine;
using System;
using System.Collections;

namespace GameJam.UI
{
  /// <summary>
  /// Uses legacy animations to animate panels forwards and reverse
  /// Add this to any panel view to play an animation on show or hide
  /// </summary>
  [RequireComponent( typeof( Animation ) ), RequireComponent( typeof( PanelController ) )]
  public class PanelAnimationController : MonoBehaviour, IPanelTransition
  {

    public enum AnimationDirection : int
    {
      Forward = 1,
      Reverse = -1
    };

    Animation _animation;
    public Animation Animation
    {
      get
      {
        if ( _animation == null )
        {
          _animation = GetComponent<Animation>();
        }
        return _animation;
      }
    }

    [Header( "Show" )]
    [SerializeField]
    AnimationClip showAnimation;
    [SerializeField]
    AnimationDirection showAnimationDirection = AnimationDirection.Forward;
    [SerializeField]
    bool _alwaysAnimate = false;
    public bool AlwaysAnimate
    {
      get
      {
        return _alwaysAnimate;
      }
    }

    [Header( "Hide" )]
    [SerializeField]
    AnimationClip hideAnimation;
    [SerializeField]
    AnimationDirection hideAnimationDirection = AnimationDirection.Forward;

    bool _animating = false;

    Action callback = null;
    float time = 0;
    AnimationDirection direction = AnimationDirection.Forward;

    public virtual void TransitionShow( Action onFinish )
    {
      if ( showAnimationDirection == AnimationDirection.Forward )
      {
        StartAnimation( showAnimation, 0, showAnimationDirection, onFinish );
      }
      else
      {
        StartAnimation( showAnimation, showAnimation.length, showAnimationDirection, onFinish );
      }
    }

    public virtual void TransitionHide( Action onFinish )
    {
      if ( hideAnimationDirection == AnimationDirection.Forward )
      {
        StartAnimation( hideAnimation, 0, hideAnimationDirection, onFinish );
      }
      else
      {
        StartAnimation( hideAnimation, hideAnimation.length, hideAnimationDirection, onFinish );
      }
    }

    protected virtual void Update()
    {
      if ( _animating )
      {
        if ( direction == AnimationDirection.Forward )
        {
          time += UnityEngine.Time.deltaTime;
          if ( time > Animation.clip.length )
          {
            time = Animation.clip.length;
          }
        }
        else
        {
          time -= UnityEngine.Time.deltaTime;
          if ( time < 0 )
          {
            time = 0;
          }
        }

        foreach ( AnimationState state in Animation )
        {
          if ( Animation.IsPlaying( state.name ) )
          {
            state.time = time;
          }
        }

        Animation.Sample();

        if ( direction == AnimationDirection.Forward )
        {
          if ( time >= Animation.clip.length )
          {
            FinishAnimation();
          }
        }
        else
        {
          if ( time <= 0 )
          {
            FinishAnimation();
          }
        }

      }
      else
      {
        enabled = false;
      }
    }

    protected void StartAnimation( AnimationClip clip, float startTime, AnimationDirection animationDirection, Action onFinish )
    {
      if ( _animating )
      {
        FinishAnimation();
      }

      if ( clip == null )
      {
        //No animation exists so just do callback
        if ( onFinish != null )
        {
          onFinish();
        }
        return;
      }

      _animating = true;
      enabled = true;

      Animation.enabled = false;
      Animation.clip = clip;

      if ( !Animation.IsPlaying( clip.name ) )
      {
        Animation.Play( clip.name );
      }

      time = startTime;
      direction = animationDirection;
      callback = onFinish;

      //Initialize
      foreach ( AnimationState state in Animation )
      {
        if ( Animation.IsPlaying( state.name ) )
        {
          state.time = time;
        }
      }

      Animation.Sample();

    }

    protected void FinishAnimation()
    {
      _animating = false;
      if ( callback != null )
      {
        callback();
        callback = null;
      }
    }

  }

}
