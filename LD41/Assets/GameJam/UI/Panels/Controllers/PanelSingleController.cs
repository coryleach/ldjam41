﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public class PanelSingleController : PanelMenuController
  {

    [SerializeField]
    PanelOptions defaultPanelOptions;

    [SerializeField]
    PanelSingleSystem panelSingleSystem;
    protected override PanelMenuSystem PanelMenuSystem
    {
      get { return panelSingleSystem; }
    }

    PanelController currentPanel = null;
    public PanelController CurrentPanel
    {
      get
      {
        return currentPanel;
      }
    }

    IPanelOptions currentPanelOptions = null;
    public IPanelOptions CurrentPanelOptions
    {
      get
      {
        return currentPanelOptions;
      }
    }

    [SerializeField]
    [Tooltip("Shows the default panel options every time this panel is shown")]
    bool showDefaultEveryTime = false;

    void Start()
    {
      if (!showDefaultEveryTime && defaultPanelOptions.PanelType != null)
      {
        panelSingleSystem.Show(defaultPanelOptions);
      }
    }

    public override void WillShow(IPanelOptions options = null)
    {
      base.WillShow(options);
      if (showDefaultEveryTime)
      {
        SubscribeToSystemTransition();
        panelSingleSystem.Show(defaultPanelOptions);
      }
    }

  }

}