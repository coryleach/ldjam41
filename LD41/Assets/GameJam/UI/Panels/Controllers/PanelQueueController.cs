﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GameJam.Events;

namespace GameJam.UI
{

  public class PanelQueueController : PanelMenuController, IGameEventListener
  {

    [SerializeField]
    PanelQueueSystem panelQueueSystem;
    protected override PanelMenuSystem PanelMenuSystem
    {
      get { return panelQueueSystem; }
    }

    protected override void Awake()
    {
      base.Awake();

      if (panelQueueSystem.onPanelEnqueued != null)
      {
        panelQueueSystem.onPanelEnqueued.AddListener(this);
      }
    }

    public void OnEventRaised()
    {
      if ( !waitingOnPanel )
      {
        panelQueueSystem.DequeuePanel();
      }
    }

    protected override void ShowPanel(PanelController showPanel, IPanelOptions options, Action callback = null)
    {
      base.ShowPanel(showPanel, options, callback);

      if ( showPanel != null )
      {
        StartCoroutine(DequeueOnDismiss(showPanel));
      }
    }

    bool waitingOnPanel = false;

    IEnumerator DequeueOnDismiss(PanelController panel)
    {
      waitingOnPanel = true;
      while ( panel.IsShowing || panel.IsTransitioning )
      {
        yield return null;
      }
      waitingOnPanel = false;
      panelQueueSystem.DequeuePanel();
    }

  }

}
