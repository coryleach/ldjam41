﻿using UnityEngine;
using System.Collections;

namespace GameJam.UI
{

	/// <summary>
	/// Interface for panel options
	/// Panel options are used to tell a panel stack or queue which panel to show and how to show it
	/// It can also be used to provide a data parameter or data context to a panel when showing it
	/// </summary>
	public interface IPanelOptions 
	{

    /// <summary>
    /// Panel type identifier
    /// </summary>
    PanelType PanelType { get; }

    /// <summary>
    /// Transition Type specifies the order of hide and show actions
    /// </summary>
    PanelTransitionType Transition { get; }

    /// <summary>
    /// True if animation should play during show transition
    /// </summary>
    bool AnimateShow { get; }

    /// <summary>
    /// True if animation should play during hide transition
    /// </summary>
    bool AnimateHide { get; }

    /// <summary>
    /// Reference to the menu the panel is belongs to
    /// </summary>
    PanelMenuController MenuController { get; set; }

	}

}
