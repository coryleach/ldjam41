﻿using UnityEngine.Events;

namespace GameJam.UI
{
  [System.Serializable]
	public class PanelEvent : UnityEvent<PanelController> { }
}