﻿using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{
  [CreateAssetMenu(menuName ="GameJam/UI/Events/Transition")]
  public class PanelTransitionEvent : ScriptableObject
  {

    List<IPanelTransitionEventListener> listeners = new List<IPanelTransitionEventListener>();

    public void AddListener(IPanelTransitionEventListener listener)
    {
      listeners.Add(listener);
    }

    public void RemoveListener(IPanelTransitionEventListener listener)
    {
      listeners.Remove(listener);
    }

    public void Raise(IPanelOptions hideOptions, IPanelOptions showOptions, PanelTransitionDirection direction = PanelTransitionDirection.Forward)
    {
      for ( int i = 0; i < listeners.Count; i++ )
      {
        listeners[i].OnPanelTransition(hideOptions, showOptions, direction);
      }
    }
    
  }

}
