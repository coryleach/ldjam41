﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public interface IPanelTransitionEventListener
  {
    void OnPanelTransition(IPanelOptions hideOptions, IPanelOptions showOptions, PanelTransitionDirection direction);
  }

}