﻿using UnityEngine;
using System.Collections;

namespace GameJam.UI
{
  /// <summary>
  /// Interface for a UI panel stack.
  /// </summary>
  public interface IPanelStack
  {

  	//Every stack has a currently showing panel
    PanelController CurrentPanel { get; }

    /// <summary>
    /// We use a panel options struct to tell the stack which panel to push and how it should be displayed
    /// </summary>
    /// <param name="panelOptions">Panel options.</param>
    void PushPanel( IPanelOptions panelOptions );

    /// <summary>
    /// Pops the top panel off the stack and reveals the next panel below in the stack
    /// </summary>
    void PopPanel();

    /// <summary>
    /// Pops the stack to a specific panel specified in the panel options
    /// </summary>
    /// <param name="panelOptions">Panel options.</param>
    void PopPanel( IPanelOptions panelOptions );

  }

}
