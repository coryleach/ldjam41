﻿using UnityEngine;
using System.Collections.Generic;

namespace GameJam.UI
{

  public class PanelProvider : MonoBehaviour, IPanelProvider
  {

    [SerializeField]
    protected List<PanelController> panels;

    Dictionary<PanelType, PanelController> panelDictionary = null;
    Dictionary<string, PanelType> panelTypeDictionary = null;

    public virtual List<PanelController> GetAllPanels()
    {
      return panels;
    }

    private void Start()
    {
      foreach (var panel in panels)
      {
        if (!panel.IsShowing)
        {
          panel.gameObject.SetActive(false);
        }
      }
    }

    void BuildPanelDictionary()
    {

      panelDictionary = new Dictionary<PanelType, PanelController>();
      panelTypeDictionary = new Dictionary<string, PanelType>();

      for (int i = 0; i < panels.Count; i++)
      {

        if (panels[i] == null)
        {
          continue;
        }

        if (panels[i].Attributes == null)
        {
          Debug.LogError("Panel " + panels[i].name + " has no attributes component. Attributes needed by panel provider.");
          continue;
        }

        var panelType = panels[i].Attributes.PanelType;
        panelTypeDictionary.Add(panelType.name, panelType);
        panelDictionary.Add(panelType, panels[i]);

      }

    }

    public PanelController GetPanel(IPanelOptions options)
    {
      if (panelDictionary == null)
      {
        BuildPanelDictionary();
      }
      return panelDictionary[options.PanelType];
    }

    public PanelType GetPanelType(string name)
    {
      if (panelTypeDictionary == null)
      {
        BuildPanelDictionary();
      }
      return panelTypeDictionary[name];
    }

  }

}