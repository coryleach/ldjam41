﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{
  public interface IPanelAttributes
  {
    PanelType PanelType { get; }
  }
}