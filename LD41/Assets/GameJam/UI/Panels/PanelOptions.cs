﻿using UnityEngine;

namespace GameJam.UI
{

  [System.Serializable]
  public class PanelOptions : IPanelOptions
  {

    static IPanelOptions _animate = new PanelOptions(true, true);
    public static IPanelOptions Animate { get { return _animate; } }

    static IPanelOptions _doNotAnimate = new PanelOptions(false, false);
    public static IPanelOptions DoNotAnimate { get { return _doNotAnimate; } }

    public PanelOptions() { }
    public PanelOptions(bool pAnimateShow = true, bool pAnimateHide = true)
    {
      animateShow = pAnimateShow;
      animateHide = pAnimateHide;
    }

    [SerializeField]
    PanelType panelType = null;
    public virtual PanelType PanelType
    {
      get
      {
        return panelType;
      }
      set
      {
        panelType = value;
      }
    }

    [SerializeField]
    PanelTransitionType transitionType;
    public PanelTransitionType Transition
    {
      get { return transitionType; }
      set { transitionType = value; }
    }

    [SerializeField]
    bool animateHide = true;
    public bool AnimateHide
    {
      get { return animateHide; }
      set { animateHide = value; }
    }

    [SerializeField]
    bool animateShow = true;
    public bool AnimateShow
    {
      get { return animateShow; }
      set { animateShow = value; }
    }

    PanelMenuController menuController;
    public PanelMenuController MenuController
    {
      get { return menuController; }
      set { menuController = value; }
    }

  }

}