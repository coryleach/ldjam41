﻿using UnityEngine;
using System;
using System.Collections;

namespace GameJam.UI
{
  /// <summary>
  /// Interface for a panel show/hide transition
  /// </summary>
  public interface IPanelTransition
  {

    void TransitionShow( Action onFinish );
    void TransitionHide( Action onFinish );

    bool AlwaysAnimate { get; }

  }

}