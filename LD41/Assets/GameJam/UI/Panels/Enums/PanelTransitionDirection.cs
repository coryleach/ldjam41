﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public enum PanelTransitionDirection
  {
    Forward,
    Backward,
  }

}
