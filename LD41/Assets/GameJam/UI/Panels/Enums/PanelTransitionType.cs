﻿
namespace GameJam.UI
{
  public enum PanelTransitionType
  {
    ShowThenHide,
    HideThenShow,
    ShowOnly
  };
}
