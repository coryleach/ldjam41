﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.UI
{

  public class PanelQueueDismiss : MonoBehaviour
  {

    [SerializeField]
    PanelQueueSystem panelQueueSystem;

    public void Dismiss()
    {
      panelQueueSystem.DismissPanel();
    }

  }

}
