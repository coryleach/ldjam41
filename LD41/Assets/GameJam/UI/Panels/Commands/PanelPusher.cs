﻿using UnityEngine;
using GameJam.Services;

namespace GameJam.UI
{

  public class PanelPusher : MonoBehaviour
  {

    [SerializeField]
    PanelStackSystem panelStack;

    public PanelOptions panelOptions;

    public void PushPanel()
    {
      panelStack.Push(panelOptions);
    }

  }

}