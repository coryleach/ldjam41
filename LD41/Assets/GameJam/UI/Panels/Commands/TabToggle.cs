﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameJam.UI
{
  public class TabToggle : MonoBehaviour, IPanelTransitionEventListener
  {

    [SerializeField]
    PanelTabSystem tabSystem;

    [SerializeField]
    PanelOptions options;

    Toggle toggle;

    public void Start()
    {
      toggle = GetComponent<Toggle>();
      toggle.onValueChanged.AddListener(Show);
      tabSystem.onPanelTransition.AddListener(this);
    }

    public void Show(bool toggleValue)
    {
      if ( toggleValue )
      {
        tabSystem.ShowTab(options);
      }
    }

    public void OnPanelTransition(IPanelOptions hideOptions, IPanelOptions showOptions, PanelTransitionDirection direction)
    {
      toggle.onValueChanged.RemoveListener(Show);
      toggle.isOn = showOptions.PanelType == options.PanelType;
      toggle.onValueChanged.AddListener(Show);
    }

  }
}