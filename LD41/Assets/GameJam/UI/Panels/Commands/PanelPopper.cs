﻿using UnityEngine;
using GameJam.Services;
using System;

namespace GameJam.UI
{

	public class PanelPopper : MonoBehaviour 
	{

    [SerializeField]
    PanelStackSystem panelStack;

		public void Pop()
		{
      panelStack.Pop();
    }

	}

}