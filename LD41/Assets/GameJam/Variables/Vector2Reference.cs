﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [System.Serializable]
  public class Vector2Reference : VariableReference<Vector2,Vector2Variable>
  {

  }
}