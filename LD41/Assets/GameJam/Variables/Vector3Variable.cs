﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName = "GameJam/Variables/Vector3")]
  public class Vector3Variable : ScriptableObject, IVariable<Vector3>
  {
    [SerializeField]
    Vector3 value;
    public Vector3 Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}