﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName = "GameJam/Variables/Int")]
  public class IntVariable : ScriptableObject, IVariable<int>
  {
    [SerializeField]
    int value;
    public int Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}
