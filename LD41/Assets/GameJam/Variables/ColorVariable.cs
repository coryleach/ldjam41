﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName = "GameJam/Variables/Color")]
  public class ColorVariable : ScriptableObject, IVariable<Color>
  {
    [SerializeField]
    Color value;
    public Color Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}