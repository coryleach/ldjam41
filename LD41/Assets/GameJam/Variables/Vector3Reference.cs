﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [System.Serializable]
  public class Vector3Reference : VariableReference<Vector3,Vector3Variable>
  {
    
  }
}