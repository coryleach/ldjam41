﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName = "GameJam/Variables/String")]
  public class StringVariable : ScriptableObject, IVariable<string>
  {
    [SerializeField]
    string value;
    public string Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}