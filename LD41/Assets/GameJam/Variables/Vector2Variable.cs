﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName = "GameJam/Variables/Vector2")]
  public class Vector2Variable : ScriptableObject, IVariable<Vector2>
  {
    [SerializeField]
    Vector2 value;
    public Vector2 Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}