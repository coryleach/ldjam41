﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [CreateAssetMenu(menuName ="GameJam/Variables/Float")]
  public class FloatVariable : ScriptableObject, IVariable<float>
  {
    [SerializeField]
    float value;
    public float Value
    {
      get { return value; }
      set { this.value = value; }
    }
  }
}