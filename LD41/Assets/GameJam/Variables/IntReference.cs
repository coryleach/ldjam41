﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  [System.Serializable]
  public class IntReference : VariableReference<int,IntVariable>
  {
    
  }
}