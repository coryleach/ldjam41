﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{

  public class ScriptableVariableTest : MonoBehaviour
  {

    public FloatReference floatVariable;
    public StringReference stringVariable;
    public IntReference intVariable;

  }

}