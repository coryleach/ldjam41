﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Variables
{
  public interface IVariable<T>
  {
    T Value { get; set; }
  }
}
