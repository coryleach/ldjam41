﻿using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Camera
{

  /// <summary>
  /// Takes care of managing a set of components that control camera movement and behavior
  /// </summary>
  [RequireComponent(typeof(UnityEngine.Camera))]
  public class CameraDirector : MonoBehaviour
  {

    /// <summary>
    /// Unity events that recieve CameraDirector as an argument
    /// </summary>
    [System.Serializable]
    public class CameraDirectorEvent : UnityEvent<CameraDirector> { }

    [SerializeField]
    CameraType cameraType;
    public CameraType CameraType
    {
      get { return cameraType; }
    }

    UnityEngine.Camera _camera;
    public UnityEngine.Camera Camera
    {
      get
      {
        if (_camera == null)
        {
          _camera = GetComponent<UnityEngine.Camera>();
        }
        return _camera;
      }
    }

    public CameraDirectorEvent OnDirectorEnabled = new CameraDirectorEvent();
    public CameraDirectorEvent OnDirectorDisabled = new CameraDirectorEvent();

    protected virtual void Awake()
    {
      CameraService.Current.Add(this);
    }

    protected virtual void OnEnable()
    {
      OnDirectorEnabled.Invoke(this);
    }

    protected virtual void OnDisable()
    {
      OnDirectorDisabled.Invoke(this);
    }

    protected virtual void OnDestroy()
    {
      CameraService.Current.Remove(this);
    }

  }

}