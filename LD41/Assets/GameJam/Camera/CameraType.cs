﻿using System.Collections;
using UnityEngine;

namespace GameJam.Camera
{

  /// <summary>
  /// This class defines a type of asset that is used to identify a camera by type
  /// </summary>
  [CreateAssetMenu(menuName ="GameJam/CameraType")]
  public class CameraType : ScriptableObject
  {
    
  }

}