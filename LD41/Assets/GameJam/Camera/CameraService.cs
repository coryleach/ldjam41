﻿using System.Collections.Generic;
using GameJam.Services;

namespace GameJam.Camera
{

  /// <summary>
  /// Camera service provides an interface for getting different camera directors that are tied to specific cameras in your scene
  /// </summary>
  public class CameraService
  {

    //Camera service gets created on-demand
    public static CameraService Current
    {
      get {
        var service = ServiceProvider.Current.Get<CameraService>();
        if ( service == null )
        {
          service = new CameraService();
          ServiceCollection.Current.Add(service);
        }
        return service;
      }
    }

    CameraDirector mainDirector;
    public CameraDirector MainCamera
    {
      get { return mainDirector; }
    }

    Dictionary<CameraType, CameraDirector> dict = new Dictionary<CameraType, CameraDirector>();

    public void Add(CameraDirector director)
    {
      dict.Add(director.CameraType, director);
      director.OnDirectorEnabled.AddListener(DirectorEnabled);
      director.OnDirectorDisabled.AddListener(DirectorDisabled);
      //Check if it's the main camera director
      DirectorEnabled(director);
    }

    public void Remove(CameraDirector director)
    {
      if ( dict.Remove(director.CameraType) )
      {
        director.OnDirectorEnabled.RemoveListener(DirectorEnabled);
        director.OnDirectorDisabled.RemoveListener(DirectorDisabled);
      }
      //This will check if it's the main director
      DirectorDisabled(director);
    }

    public CameraDirector Get(CameraType type)
    {
      return dict[type];
    }

    void DirectorEnabled(CameraDirector director)
    {
      if (UnityEngine.Camera.main != null && director.gameObject == UnityEngine.Camera.main.gameObject)
      {
        mainDirector = director;
      }
    }

    void DirectorDisabled(CameraDirector director)
    {
      if (director == mainDirector)
      {
        mainDirector = null;
      }
    }

  }
}