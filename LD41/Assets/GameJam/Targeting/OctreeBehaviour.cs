﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Targeting
{

  public class OctreeBehaviour : MonoBehaviour, IOctreeItem<OctreeBehaviour>
  {

    List<OctreeNode<OctreeBehaviour>> ownerNodes = new List<OctreeNode<OctreeBehaviour>>();
    public IList<OctreeNode<OctreeBehaviour>> OwnerNodes
    {
      get { return ownerNodes; }
    }

    Vector3 previousPosition;

    public Vector3 Position
    {
      get { return previousPosition; }
    }

    void Start()
    {
      previousPosition = transform.position;
      RefreshTree();
    }

    void Update()
    {
      UpdatePosition();
    }

    void UpdatePosition()
    {
      if (previousPosition != transform.position)
      {
        previousPosition = transform.position;
        RefreshTree();
      }
    }

    List<OctreeNode<OctreeBehaviour>> obsoleteOwners = new List<OctreeNode<OctreeBehaviour>>();

    void RefreshTree()
    {
      OctreeController.Current.Octree.Add(this);

      for ( int i = 0; i < OwnerNodes.Count; i++ )
      {
        var node = OwnerNodes[i];
        if ( !node.ContainsPosition(Position) )
        {
          obsoleteOwners.Add(node);
        }
      }

      for ( int i = 0; i < obsoleteOwners.Count; i++ )
      {
        var node = obsoleteOwners[i];
        node.Items.Remove(this);
        node.Reduce();
        ownerNodes.Remove(node);
      }

      obsoleteOwners.Clear();
    }
    public bool DrawOwner = false;

    private void OnDrawGizmos()
    {
      if ( !DrawOwner )
      {
        return;
      }

      Gizmos.color = Color.red;
      for ( int i = 0; i < OwnerNodes.Count; i++ )
      {
        var node = OwnerNodes[i];
        Vector3 size = new Vector3(node.Size, node.Size, node.Size);
        Gizmos.DrawWireCube(node.Position, size);
      }
      
    }

  }

}