﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Services;

namespace GameJam.Targeting
{

  public class OctreeController : MonoBehaviour
  {

    public static OctreeController Current
    {
      get { return ServiceProvider.Current.Get<OctreeController>(); }
    }

    Octree<OctreeBehaviour> octree = null;
    public Octree<OctreeBehaviour> Octree
    {
      get { return octree; }
    }

    [SerializeField]
    float size = 100;

    private void Awake()
    {
      octree = new Octree<OctreeBehaviour>(transform.position, size);
      ServiceCollection.Current.Add(this);
    }

    void Start()
    {
      
    }

    private void OnDrawGizmos()
    {

      if ( !Application.isPlaying )
      {
        return;
      }

      DrawNode(octree.RootNode);

    }

    private void DrawNode(OctreeNode<OctreeBehaviour> node)
    {
      Vector3 size = new Vector3(node.Size, node.Size, node.Size);
      Gizmos.DrawWireCube(node.Position, size);

      //Draw children if this is not a leaf node
      if ( !node.IsLeaf )
      {
        for ( int i = 0; i < node.Children.Length; i++ )
        {
          DrawNode(node.Children[i]);
        }
      }
    }

  }

}