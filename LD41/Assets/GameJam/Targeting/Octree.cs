﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Targeting
{

  public static class OctreePosition
  {
    public const int RIGHT = 1;
    public const int FRONT = 2;
    public const int UPPER = 4;
  }

  public interface IOctreeItem<TType> where TType : IOctreeItem<TType>
  {
    Vector3 Position { get; }
    IList<OctreeNode<TType>> OwnerNodes { get; }
  }

  [System.Serializable]
  public class Octree<TType> where TType : IOctreeItem<TType>
  {

    private OctreeNode<TType> rootNode;
    public OctreeNode<TType> RootNode
    {
      get { return rootNode; }
    }

    public Octree(int size = 100)
    {
      rootNode = new OctreeNode<TType>(null, Vector3.zero, size);
    }

    public Octree(Vector3 position, float size)
    {
      rootNode = new OctreeNode<TType>(null, position, size);
    }

    public bool Add(TType item)
    {
      return rootNode.Add(item);
    }

    /// <summary>
    ///  First bit = Upper or Lower
    ///  Second bit = Front or Back
    ///  Third bit = Left or Right
    ///  ---------------------------
    ///  UpperBackLeft   = 0, //000
    ///  UpperBackRight  = 1, //001
    ///  UpperFrontLeft  = 2, //010
    ///  UpperFrontRight = 3, //011
    ///  LowerBackLeft   = 4, //100
    ///  LowerBackRight  = 5, //101
    ///  LowerFrontLeft  = 6, //110
    ///  LowerFrontRight = 7, //111
    /// </summary>
    /// <param name="nodePosition">Position of the node</param>
    /// <param name="targetPosition">Target position</param>
    /// <returns>Integer representing the relative position of the node</returns>
    int GetRelativePosition(Vector3 nodePosition, Vector3 targetPosition)
    {
      int position = 0;

      if ( targetPosition.y > nodePosition.y )
      {
        position |= OctreePosition.UPPER;
      }

      if ( targetPosition.x > nodePosition.x )
      {
        position |= OctreePosition.RIGHT;
      }

      if ( targetPosition.z > nodePosition.z )
      {
        position |= OctreePosition.FRONT;
      }

      return position;
    }
    
  }

  [System.Serializable]
  public class OctreeNode<TType> where TType : IOctreeItem<TType>
  {
    const int maxItemCount = 1;

    Vector3 position;
    public Vector3 Position
    {
      get { return position; }
    }

    float size;
    public float Size
    {
      get { return size; }
    }

    OctreeNode<TType> parent;
    public OctreeNode<TType> Parent
    {
      get { return parent; }
    }

    //Children will be cached here so that if we convert from a leaf node and back again we don't perform any unneeded allocations
    OctreeNode<TType>[] cachedChildren;

    OctreeNode<TType>[] children;
    public OctreeNode<TType>[] Children
    {
      get { return children; }
    }

    IList<TType> items;
    public IList<TType> Items
    {
      get { return items; }
    }

    public bool IsLeaf
    {
      get { return children == null; }
    }

    public OctreeNode(OctreeNode<TType> parent, Vector3 position, float size)
    {
      this.parent = parent;
      this.position = position;
      this.size = size;
      items = new List<TType>();
    }

    public bool Add(TType item)
    {
      if (!ContainsPosition(item.Position))
      {
        return false;
      }

      if (IsLeaf)
      {
        PushItem(item);
        return true;
      }
      else
      {
        for (int i = 0; i < children.Length; i++)
        {
          if (children[i].Add(item))
          {
            return true;
          }
        }
      }

      return false;
    }

    void PushItem(TType item)
    {
      if (!Items.Contains(item))
      {
        //Add to this list
        Items.Add(item);
        item.OwnerNodes.Add(this);
      }

      //Subdivide if we've hit the max nodes per octant
      if (Items.Count > maxItemCount)
      {
        Subdivide();
      }
    }

    public bool ContainsPosition(Vector3 pt)
    {
      float halfSize = size * 0.5f;

      if (pt.x > position.x + halfSize || pt.x < position.x - halfSize)
      {
        return false;
      }

      if (pt.y > position.y + halfSize || pt.y < position.y - halfSize)
      {
        return false;
      }

      if (pt.z > position.z + halfSize || pt.z < position.z - halfSize)
      {
        return false;
      }

      return true;
    }

    void Subdivide()
    {
      if ( children == null && cachedChildren != null )
      {
        children = cachedChildren;
      }
      else if (children == null && cachedChildren == null)
      {
        children = new OctreeNode<TType>[8];

        for (int i = 0; i < 8; i++)
        {
          Vector3 newPosition = position;

          if ((i & OctreePosition.UPPER) != 0)
          {
            //Upper
            newPosition.y += size * 0.25f;
          }
          else
          {
            //Lower
            newPosition.y -= size * 0.25f;
          }

          if ((i & OctreePosition.FRONT) != 0)
          {
            //Front
            newPosition.z += size * 0.25f;
          }
          else
          {
            //Back
            newPosition.z -= size * 0.25f;
          }

          if ((i & OctreePosition.RIGHT) != 0)
          {
            //Right
            newPosition.x += size * 0.25f;
          }
          else
          {
            //Left
            newPosition.x -= size * 0.25f;
          }

          children[i] = new OctreeNode<TType>(this, newPosition, size * 0.5f);
        }
      }

      //Move all items into child nodes
      for (int i = 0; i < Items.Count; i++)
      {
        var item = Items[i];
        item.OwnerNodes.Remove(this);
        for (int j = 0; j < children.Length; j++)
        {
          if (children[j].Add(item))
          {
            break;
          }
        }
      }

      //Clear this node's item list after moving all children to leaf nodes
      Items.Clear();
    }

    public void Reduce()
    {
      if ( IsLeaf )
      {
        //Try to reduce the parent if we are a leaf and we have fewer than max items
        if (Items.Count <= maxItemCount && Parent != null)
        {
          Parent.Reduce();
        }
      }
      else if ( !IsLeaf )
      {
        if ( CountItems() <= maxItemCount )
        {
          //We can reduce
          CollectChildren(this);
          children = null;
          //Try to reduce this parent now as well
          if ( Parent != null )
          {
            Parent.Reduce();
          }
        }
      }
    }

    /// <summary>
    /// Counts all items in this node and in all children beneath this node
    /// </summary>
    /// <returns></returns>
    int CountItems()
    {
      int sum = 0;

      if ( IsLeaf )
      {
        sum += Items.Count;
      }
      else
      {
        for (int i = 0; i < children.Length; i++)
        {
          sum += children[i].CountItems();
        }
      }

      return sum;
    }

    /// <summary>
    /// Gets all the items in the child nodes and adds them to this nodes list
    /// </summary>
    /// <param name="node"></param>
    void CollectChildren(OctreeNode<TType> node)
    {
      if ( node.IsLeaf )
      {
        for ( int i = 0; i < node.Items.Count; i++ )
        {
          node.Items[i].OwnerNodes.Remove(node);
          Items.Add(node.Items[i]);
          node.Items[i].OwnerNodes.Add(this);
        }
        node.Items.Clear();
      }
      else
      {
        for ( int i = 0; i < node.Children.Length; i++ )
        {
          CollectChildren(node.Children[i]);
        }
      }
    }

    OctreeNode<TType> NodeAtPosition(Vector3 pt)
    {
      return null;
    }

    void FindNeighbors()
    {
      //Get tree root
      var root = this;
      while ( root.parent != null )
      {
        root = root.parent;
      }

      

    }

  }

}
