﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.RuntimeSets
{
  [CreateAssetMenu(menuName ="GameJam/Sets/GameObjectSet")]
  public class GameObjectRuntimeSet : RuntimeSet<GameObject>
  {

  }

}