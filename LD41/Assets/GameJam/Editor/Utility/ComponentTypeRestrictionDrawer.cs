﻿using UnityEngine;
using UnityEditor;
using System.Collections;

namespace GameJam.Utility
{

	[CustomPropertyDrawer(typeof(ComponentTypeRestrictionAttribute))]
	public class ComponentTypeRestrictionDrawer : PropertyDrawer {

		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			var typeRestriction = attribute as ComponentTypeRestrictionAttribute;
			var type = typeRestriction.inheritsFromType;

			EditorGUI.PropertyField(position,property,label,false);

			if ( property.objectReferenceValue != null )
			{
				var component = property.objectReferenceValue as Component;
				var gameObject = component.gameObject;
				var finalComponent = gameObject.GetComponent(type);

				if ( finalComponent == null )
				{
					Debug.LogWarning("Property can only be set to an object with component of type " + type.ToString());
				}
				property.objectReferenceValue = finalComponent;
			}
		}

	}

}