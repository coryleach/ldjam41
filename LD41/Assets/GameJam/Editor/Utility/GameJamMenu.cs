﻿using UnityEngine;
using UnityEditor;

namespace GameJam.Editor
{

	public class GameJamMenu : UnityEditor.Editor 
	{

		[MenuItem("GameJam/PlayerPrefs/DeleteAll",false,12)]
		public static void ClearGameData()
		{
			PlayerPrefs.DeleteAll();
			EditorUtility.DisplayDialog("Cleared!", "Player Prefs Cleared", "Ok");	
		}

	}

}