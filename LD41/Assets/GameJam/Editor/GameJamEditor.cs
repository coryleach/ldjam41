﻿using UnityEngine;
using UnityEditor;
using System.Reflection;

[InitializeOnLoad]
public class GameJamEditor
{
	static GameJamEditor()
  {
    Debug.Log("Game Jam Editor Initialized");

    //Replace float format strings to replace weird values in editor fields
    typeof(EditorGUI).GetField("kFloatFieldFormatString", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, "0.#######");
    typeof(EditorGUI).GetField("kDoubleFieldFormatString", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, "0.###############");
  }
}
