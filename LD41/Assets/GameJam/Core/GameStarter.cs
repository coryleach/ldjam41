﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameJam.Services;

namespace GameJam.Core
{
  public class GameStarter : MonoBehaviour
  {

    public UnityEvent OnBegin = new UnityEvent();
    public UnityEvent OnComplete = new UnityEvent();

    IEnumerator Start()
    {

      DontDestroyOnLoad(gameObject);

      OnBegin.Invoke();

      //Initialize new service provider and collection
      var serviceProvider = new BasicServiceProvider();
      ServiceCollection.Current = serviceProvider;
      ServiceProvider.Current = serviceProvider;

      OnComplete.Invoke();

      yield return null;

      //Finally Destroy this game object
      Destroy(gameObject);

    }

  }
}