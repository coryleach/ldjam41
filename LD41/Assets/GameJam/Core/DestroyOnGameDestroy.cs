﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Core
{
  public class DestroyOnGameDestroy : MonoBehaviour
  {

    void Awake()
    {
      GameDestroyer.OnDestroy.AddListener(GameDestroy);
    }

    void GameDestroy()
    {
      Destroy(gameObject);
    }
   
  }
}