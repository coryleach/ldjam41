﻿using UnityEngine;
using System.Collections;

namespace GameJam.Core 
{
  
  public abstract class GameController<T> : MonoBehaviour where T : GameBase
  {

    public abstract T Game { get; protected set; }

    [SerializeField]
    protected string savedGameKey = "_SavedGame_";

    protected virtual void Save()
    {
        var jsonData = JsonUtility.ToJson(Game);
        PlayerPrefs.SetString(savedGameKey, jsonData);
    }

    protected virtual bool Load()
    {
        var jsonData = PlayerPrefs.GetString(savedGameKey, string.Empty);
        if (!string.IsNullOrEmpty(jsonData))
        {
            Game = JsonUtility.FromJson<T>(jsonData);
            return true;
        }
        else
        {
            return false;
        }
    }
    
  }

}