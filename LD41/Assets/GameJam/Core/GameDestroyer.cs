﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameJam.Services;

namespace GameJam.Core
{

  public class GameDestroyer : MonoBehaviour
  {
    //Any object that needs to do something on game destroy should subscribe here
    public static UnityEvent OnDestroy = new UnityEvent();

    public UnityEvent OnDestroyStart = new UnityEvent();
    public UnityEvent OnDestroyComplete = new UnityEvent();

    IEnumerator Start()
    {
      DontDestroyOnLoad(this);

      OnDestroyStart.Invoke();

      //Tear down all do not destroy objects
      //Apply DestroyOnGameDestroy component to objects you want to destroy themselves on game destroy
      OnDestroy.Invoke();

      //Clear all listeners. This is important becasue OnDestroy is static
      OnDestroy.RemoveAllListeners();

      //Clear out any statics
      ServiceProvider.Current = null;
      ServiceCollection.Current = null;

      //TODO: Force Garbage Collect?
      
      OnDestroyComplete.Invoke();

      yield return null;

      //Finall Destroy this object
      Destroy(gameObject);
    }

  }

}