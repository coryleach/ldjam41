﻿
namespace GameJam.Services
{

  public static class ServiceProvider
  {
    
    static IServiceProvider current;
    public static IServiceProvider Current
    {
      get
      {
        if ( current == null )
        {
          return BasicServiceProvider.Current;
        }
        return current;
      }
      set
      {
        current = value;
      }
    }

  }

}