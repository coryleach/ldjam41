﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Services
{

  /// <summary>
  /// Interface for handling transitions between game modes
  /// </summary>
  public interface IGameModeTransitioner
  {

    /// <summary>
    /// This function should become a deferred call that kicks off a transition
    /// This could play an animation to fade between scenes or whatever
    /// </summary>
    /// <param name="previousMode"></param>
    /// <param name="newMode"></param>
    void PreTransition(GameMode previousMode, GameMode newMode);

    /// <summary>
    /// Same as pre transition but happens after the game mode transition instead of before
    /// </summary>
    /// <param name="previousMode"></param>
    /// <param name="newMode"></param>
    void PostTransition(GameMode previousMode, GameMode newMode);

  }

}