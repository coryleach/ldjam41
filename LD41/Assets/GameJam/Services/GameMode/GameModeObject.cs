﻿using UnityEngine;
using System.Collections.Generic;

namespace GameJam.Services
{

  public class GameModeObject : MonoBehaviour
  {

    public List<GameMode> modes;

    void Start()
    {
      GameModeManager.Current.OnGameModeChanged.AddListener(OnGameModeChanged);
    }

    void OnGameModeChanged(GameMode mode)
    {
      gameObject.SetActive(modes.Contains(mode));
    }

  }

}
