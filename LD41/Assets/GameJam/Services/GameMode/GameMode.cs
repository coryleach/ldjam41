﻿using UnityEngine;

namespace GameJam.Services
{
  [CreateAssetMenu(menuName ="GameJam/Services/GameMode")]
  [System.Serializable]
  public class GameMode : ScriptableObject
  {

  }
}