﻿using UnityEngine;
using UnityEngine.Events;
using System;

namespace GameJam.Services
{

  [System.Serializable]
  public class GameModeEvent : UnityEvent<GameMode> { }

  public class GameModeManager : MonoBehaviour
  {

    public static GameModeManager Current
    {
      get { return ServiceProvider.Current.Get<GameModeManager>(); }
    }

    [SerializeField]
    GameMode defaultMode;

    GameMode currentMode;
    public GameMode CurrentMode
    {
      get { return currentMode; }
      private set
      {
        if (currentMode != value)
        {
          currentMode = value;
          OnGameModeChanged.Invoke(currentMode);
        }
      }
    }

    [SerializeField]
    GameModeEvent onGameModeWillChange = new GameModeEvent();
    public GameModeEvent OnGameModeWillChange { get { return onGameModeWillChange; } }

    [SerializeField]
    GameModeEvent onGameModeChanged = new GameModeEvent();
    public GameModeEvent OnGameModeChanged { get { return onGameModeChanged; } }

    void Awake()
    {
      ServiceCollection.Current.Add(this);
    }

    void Start()
    {
      CurrentMode = defaultMode;
    }

    public void TransitionToMode(GameMode mode, Action callback = null)
    {
      OnGameModeWillChange.Invoke(mode);

      CurrentMode = mode;

      if (callback != null) 
      {
        callback.Invoke();
      }
    }

  }

}