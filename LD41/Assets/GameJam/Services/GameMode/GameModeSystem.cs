﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Services;

namespace GameJam.Systems
{

  [CreateAssetMenu(menuName ="GameJam/GameModeSystem")]
  public class GameModeSystem : ScriptableObject
  {

    public GameMode defaultMode;

    GameMode currentMode;
    public GameMode CurrentMode
    {
      get { return currentMode; }
      private set { currentMode = value; }
    }

    public void TransitionToMode(GameMode targetMode)
    {
      CurrentMode = targetMode;
    }

    void BeginTransition(GameMode oldMode, GameMode newMode)
    {

    }

  }

}