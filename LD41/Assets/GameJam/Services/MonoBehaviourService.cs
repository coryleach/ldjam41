﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Services
{

  public class MonoBehaviourService<T> : MonoBehaviour where T : MonoBehaviourService<T>
  {

    public ScriptableServiceCollection serviceCollection;

    protected virtual void Awake()
    {
      serviceCollection.Add<T>(this as T);
    }

  }

}