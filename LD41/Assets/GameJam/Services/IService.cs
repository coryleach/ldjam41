﻿using UnityEngine;
using System.Collections;

namespace GameJam.Services
{
  public interface IService
  {
    string ServiceName { get; }
  }
}