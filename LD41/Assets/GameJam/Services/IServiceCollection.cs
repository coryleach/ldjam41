﻿using UnityEngine;
using System.Collections;

namespace GameJam.Services
{
  public interface IServiceCollection
  {
    void Add<T>(T service) where T : class;
    bool Remove<T>(T service) where T : class;
  }
}
