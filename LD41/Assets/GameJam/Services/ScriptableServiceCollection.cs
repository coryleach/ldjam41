﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Services
{
  [CreateAssetMenu(menuName ="GameJam/Services/ScriptableServiceCollection")]
  public class ScriptableServiceCollection : ScriptableObject, IServiceCollection, IServiceProvider
  {

    Dictionary<Type, object> dictionary = new Dictionary<Type, object>();

    public void Add<T>(T service) where T : class
    {
      dictionary.Add(service.GetType(), service);
    }

    public bool Remove<T>(T service) where T : class
    {
      if ( dictionary.ContainsKey(service.GetType()) )
      {
        var containedService = dictionary[service.GetType()];
        if ( service == containedService )
        {
          dictionary.Remove(service.GetType());
          return true;
        }
      }
      return false;
    }

    public T Get<T>() where T : class
    {
      object value;
      if (dictionary.TryGetValue(typeof(T), out value))
      {
        return value as T;
      }
      return null;
    }

  }

}