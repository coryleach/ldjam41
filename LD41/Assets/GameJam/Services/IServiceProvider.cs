﻿using UnityEngine;
using System.Collections;

namespace GameJam.Services
{
  public interface IServiceProvider
  {
    T Get<T>() where T : class;
  }
}
