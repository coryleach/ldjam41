﻿
namespace GameJam.Services
{

	public static class ServiceCollection 
	{

		static IServiceCollection current;
		public static IServiceCollection Current
		{
			get 
			{
				if ( current == null )
				{
					return BasicServiceProvider.Current;
				}
				return current;
			}
			set 
			{
				current = value;
			}
		}
		
	}

}
