﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Services
{

  public class Service<T> : MonoBehaviour where T : class
  {

    public static T Current
    {
      get { return ServiceProvider.Current.Get<T>(); }
    }

    protected virtual void Awake()
    {
      Debug.Assert((this as T) != null,"T must inherit from Service<T>");
      ServiceCollection.Current.Add<T>(this as T);
    }

    protected virtual void OnDestroy()
    {
      ServiceCollection.Current.Remove(this as T);
    }

  }

}