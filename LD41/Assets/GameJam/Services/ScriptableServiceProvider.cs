﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Services
{

  [CreateAssetMenu(menuName = "GameJam/Services/ScriptableServiceProvider")]
  public class ScriptableServiceProvider : ScriptableObject, IServiceProvider
  {

    public ScriptableServiceCollection collection;

    public T Get<T>() where T : class
    {
      return collection.Get<T>();
    }

  }

}