﻿using System.Collections.Generic;
using System;

namespace GameJam.Services
{
  
  public class BasicServiceProvider : IServiceProvider, IServiceCollection
  {

    static BasicServiceProvider current;
    public static BasicServiceProvider Current
    {
      get 
      { 
        if ( current == null )
        {
          current = new BasicServiceProvider();
        }
        return current; 
      }
      set
      {
        current = value;
      }
    }

    Dictionary<Type, object> dictionary = new Dictionary<Type, object>();

    public T Get<T>() where T : class
    {
      object value;
      if (dictionary.TryGetValue(typeof(T), out value))
      {
        return value as T;
      }
      return null;
    }

    public void Add<T>(T service) where T : class
    {
      dictionary[ typeof(T) ] = service;
    }

    public bool Remove<T>(T service) where T : class
    {
      if ( dictionary.ContainsKey(typeof(T)) && dictionary[typeof(T)] == (service as object) )
      {
        return dictionary.Remove(typeof(T));
      }
      return false;
    }

  }

}
