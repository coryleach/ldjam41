﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Events
{

  public interface IGameEventListener
  {
    void OnEventRaised();
  }

}
