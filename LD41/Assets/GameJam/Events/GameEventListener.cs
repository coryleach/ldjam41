﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameJam.Events
{
  public class GameEventListener : MonoBehaviour, IGameEventListener
  {
    public GameEvent gameEvent;
    public UnityEvent action;

    private void OnEnable()
    {
      gameEvent.AddListener(this);
    }

    private void OnDisable()
    {
      gameEvent.RemoveListener(this);
    }

    public void OnEventRaised()
    {
      action.Invoke();
    }
  }
}
