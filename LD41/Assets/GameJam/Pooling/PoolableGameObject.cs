﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Pooling
{
  public class PoolableGameObject : MonoBehaviour
  {

    Pool _pool = null;
    public Pool SourcePool
    {
      get { return _pool; }
      set { _pool = value; }
    }

    public virtual void OnPoolableSpawned()
    {

    }

    public virtual void OnPoolableDespawn()
    {

    }

    public void Despawn()
    {
      SourcePool.Despawn(this);
    }
  }
}