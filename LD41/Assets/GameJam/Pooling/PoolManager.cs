﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Services;

namespace GameJam.Pooling
{

  public class PoolManager : MonoBehaviour
  {

    public static PoolManager Current
    {
      get { return ServiceProvider.Current.Get<PoolManager>(); }
    }

    Dictionary<PoolableGameObject, Pool> poolDictionary = new Dictionary<PoolableGameObject, Pool>();

    private void Awake()
    {
      ServiceCollection.Current.Add(this);
    }

    public Pool GetPool(PoolableGameObject prefab)
    {
      Pool pool = null;

      if ( poolDictionary.TryGetValue(prefab,out pool) )
      {
        return pool;
      }

      pool = new Pool(prefab);

      poolDictionary.Add(prefab, pool);

      return pool;
    }

  }

}

