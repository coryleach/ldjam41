﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Pooling
{

  public class PoolableParticleSystem : PoolableGameObject
  {

    [SerializeField]
    ParticleSystem _particleSystem;
    public ParticleSystem ParticleSystem
    {
      get {
        if ( _particleSystem == null )
        {
          _particleSystem = GetComponent<ParticleSystem>();
        }
        return _particleSystem;
      }
    }

    public bool checkAliveWithChildren = true;

    private void Update()
    {
      if ( ParticleSystem == null )
      {
        Debug.LogError("Poolable Particle System has no particle system");
        enabled = false;
      }
      else if ( !ParticleSystem.IsAlive(checkAliveWithChildren) )
      {
        enabled = false;
        Despawn();
      }
    }

    public override void OnPoolableSpawned()
    {
      base.OnPoolableSpawned();
      enabled = true;
    }

  }

}