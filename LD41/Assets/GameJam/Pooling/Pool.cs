﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Pooling
{
  public class Pool
  {

    public Pool(PoolableGameObject prefab)
    {
      _prefab = prefab;
    }

    PoolableGameObject _prefab;
    public PoolableGameObject Prefab
    {
      get { return _prefab; }
    }

    Queue<PoolableGameObject> instanceQueue = new Queue<PoolableGameObject>();

    public PoolableGameObject Spawn(Transform parentTransform = null)
    {
      PoolableGameObject spawnedObject = null;

      if ( instanceQueue.Count > 0 )
      {
        spawnedObject = instanceQueue.Dequeue();
      }
      else
      {
        if ( parentTransform != null )
        {
          spawnedObject = Object.Instantiate(_prefab,parentTransform);
        }
        else
        {
          spawnedObject = Object.Instantiate(_prefab);
        }
        spawnedObject.SourcePool = this;
      }

      if ( Prefab.gameObject.activeSelf )
      {
        spawnedObject.gameObject.SetActive(true);
      }

      spawnedObject.OnPoolableSpawned();

      return spawnedObject;
    }

    public void Despawn(PoolableGameObject spawnedObject)
    {
      Debug.Assert(spawnedObject.SourcePool == this, "Attempting to despawn an object to a pool that is not its spawn source");

      spawnedObject.OnPoolableDespawn();
      spawnedObject.gameObject.SetActive(false);
      instanceQueue.Enqueue(spawnedObject);
    }

  }
}