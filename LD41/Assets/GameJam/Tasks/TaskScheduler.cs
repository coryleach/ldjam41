﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using GameJam.Deferrable;
using GameJam.Services;

namespace GameJam.Tasks
{

  public interface ITask
  {
    void Invoke();
  }

  public class Task : ITask
  {

    Action action;
    TaskScheduler scheduler;
    Deferred deferred;
    Exception failException;

    public Task(Action action, TaskScheduler scheduler)
    {
      this.action = action;
      deferred = new Deferred();
    }

    public IPromise Promise
    {
      get { return deferred.Promise; }
    }

    public void Invoke()
    {
      try
      {
        action.Invoke();
        scheduler.ExecuteOnMainThread(Complete);
      }
      catch (Exception exception)
      {
        failException = exception;
        scheduler.ExecuteOnMainThread(Fail);
      }
    }

    void Complete()
    {
      deferred.Complete();
    }

    void Fail()
    {
      deferred.Fail(failException);
    }

  }

  public class Task<T> : ITask
  {

    Func<T> action;
    Deferred<T> deferred;
    TaskScheduler scheduler;

    T result;
    Exception failException;

    public Task(Func<T> action, TaskScheduler scheduler)
    {
      this.action = action;
      this.scheduler = scheduler;
      deferred = new Deferred<T>();
    }

    public IPromise<T> Promise
    {
      get { return deferred.Promise; }
    }

    public void Invoke()
    {
      try
      {
        result = action.Invoke();
        scheduler.ExecuteOnMainThread(Complete);
      }
      catch (Exception exception)
      {
        this.failException = exception;
        scheduler.ExecuteOnMainThread(Fail);
      }
    }

    void Complete()
    {
      deferred.Complete(result);
    }

    void Fail()
    {
      deferred.Fail(failException);
    }

  }

  public class TaskScheduler : MonoBehaviour
  {

    public const int ThreadSleepTime = 250;

    public static TaskScheduler Current
    {
      get { return ServiceProvider.Current.Get<TaskScheduler>(); }
    }

    public int minThreadCount = 1;
    public int maxThreadCount = 3;

    Thread mainThread = null;
    public Thread MainThread
    {
      get { return mainThread; }
    }

    public bool IsMainThread
    {
      get { return Thread.CurrentThread == mainThread; }
    }

    Queue<Action> mainThreadQueue = new Queue<Action>();
    Queue<ITask> taskQueue = new Queue<ITask>();
    List<Thread> threads = new List<Thread>();

    private void Awake()
    {
      mainThread = Thread.CurrentThread;
      ServiceCollection.Current.Add(this);
    }

    private void Start()
    {
      CreateThreads();
    }

    private void OnDestroy()
    {
      StopAllThreads();
    }

    private void Update()
    {
      //Do nothing if main thread action queue is empty
      if (mainThreadQueue.Count <= 0)
      {
        return;
      }

      Action task = null;
      lock (mainThreadQueue)
      {
        task = mainThreadQueue.Dequeue();
      }
      task.Invoke();
    }

    public void StopAllThreads()
    {
      for (int i = 0; i < threads.Count; i++)
      {
        if (threads[i].IsAlive)
        {
          threads[i].Abort();
        }
      }
      threads.Clear();
    }

    public void ExecuteOnMainThread(Action task)
    {
      lock (mainThreadQueue)
      {
        mainThreadQueue.Enqueue(task);
      }
    }

    public void EnqueueTask(Action task)
    {
      lock (taskQueue)
      {
        taskQueue.Enqueue(new Task(task,this));
        CheckThreadCount();
      }
    }

    public IPromise<T> EnqueueTask<T>(Func<T> task)
    {
      lock (taskQueue)
      {
        var threadTask = new Task<T>(task, this);
        taskQueue.Enqueue(threadTask);
        CheckThreadCount();
        return threadTask.Promise;
      }
    }

    private void CreateThreads()
    {
      while (threads.Count < minThreadCount)
      {
        CreateThread();
      }
    }

    private void CreateThread()
    {
      Thread thread = new Thread(ThreadUpdate);
      thread.IsBackground = true;

      threads.Add(thread);

      thread.Start();
    }

    private void CheckThreadCount()
    {
      CleanupThreadList();

      if (taskQueue.Count >= 2 && threads.Count < maxThreadCount)
      {
        CreateThread();
      }
    }

    private void CleanupThreadList()
    {
      for (int i = threads.Count - 1; i >= 0; i--)
      {
        if (!threads[i].IsAlive)
        {
          threads.RemoveAt(i);
        }
      }
    }

    /// <summary>
    /// This code runs on a separate thread
    /// </summary>
    private void ThreadUpdate()
    {

      while (true)
      {

        //Dequeue and run tasks until we run out of tasks
        ITask task;

        do
        {
          task = null;

          lock (taskQueue)
          {
            if (taskQueue.Count > 0)
            {
              task = taskQueue.Dequeue();
            }
          }

          if (task != null)
          {
            task.Invoke();
          }

        } while (task != null);

        //Everytime we check for a task and don't find one sleep thread for some amount of time
        Thread.Sleep(ThreadSleepTime);

      }

    }


  }

}
