﻿using UnityEngine;
using System.Collections;

namespace GameJam.Time
{

  public static class GameTime
  {
    public static float scale = 1;

    public static float DeltaTime
    {
      get
      {
        return UnityEngine.Time.deltaTime * GameTime.scale;
      }
    }

  }

}
