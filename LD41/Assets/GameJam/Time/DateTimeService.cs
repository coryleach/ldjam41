﻿using System;
using UnityEngine;
using UnityEngine.Events;
using GameJam.Services;

namespace GameJam.Time
{

  public class DateTimeService : MonoBehaviour, IDateTimeService
  {

    public static IDateTimeService Current
    {
      get { return ServiceProvider.Current.Get<IDateTimeService>(); }
    }

    private TimeSpan masterTimeOffset = TimeSpan.Zero;
    private DateTime baseMasterTime;
    private long baseMilliseconds;

    private Func<long> tickCountGetter = () => { return Environment.TickCount; };
    public Func<long> TickCountGetter
    {
      get { return tickCountGetter; }
      set
      {
        tickCountGetter = value;
        SetMasterTimeOffset(masterTimeOffset);
      }
    }

    [SerializeField]
    UnityEvent onDateTimeChanged = new UnityEvent();
    public UnityEvent OnDateTimeChanged { get { return onDateTimeChanged; } }

    public void SetMasterTimeOffset(TimeSpan masterTimeOffset)
    {
      baseMilliseconds = TickCountGetter();
      baseMasterTime = DateTime.UtcNow + masterTimeOffset;
      OnDateTimeChanged.Invoke();
    }

    public virtual DateTime Now { get { return UtcNow.ToLocalTime(); } }

    public virtual DateTime UtcNow
    {
      get { return baseMasterTime + TimeSpan.FromMilliseconds(TickCountGetter() - baseMilliseconds); }
    }

    public virtual long UtcNowTicks
    {
      get { return UtcNow.Ticks; }
    }

    protected virtual void Start()
    {
      ServiceCollection.Current.Add<IDateTimeService>(this);
      SetMasterTimeOffset(TimeSpan.Zero);
    }

  }

}
