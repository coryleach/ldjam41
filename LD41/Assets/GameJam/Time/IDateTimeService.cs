﻿using System;

namespace GameJam.Time
{

  public interface IDateTimeService
  {

    DateTime Now { get; }

    DateTime UtcNow { get; }

    long UtcNowTicks { get; }

  }

}