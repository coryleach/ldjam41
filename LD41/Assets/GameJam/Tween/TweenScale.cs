﻿using UnityEngine;
using System.Collections;

namespace GameJam.Tween
{

  public class TweenScale : Tweener
  {

    public Vector3 start;
    public Vector3 end;

    protected override void Sample(float t)
    {
      Vector3 scale = Vector3.one;
      scale.x = Ease(t, start.x, end.x);
      scale.y = Ease(t, start.y, end.y);
      scale.z = Ease(t, scale.z, end.z);
      this.transform.localScale = scale;
    }

  }

}
