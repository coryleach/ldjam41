﻿using UnityEngine;
using System.Collections;

namespace GameJam.Tween
{

  public abstract class Tweener : MonoBehaviour
  {

    public enum LoopType
    {
      None,
      Loop,
      PingPong
    };

    public float delay = 0f;
    public float duration = 1f;
    public LoopType looping = LoopType.None;
    public EaseType easing = EaseType.Linear;

    float time = 0;
    float delayTime = 0;
    float direction = 1f;

    #region Monobehavior

    void Start()
    {
      delayTime = 0;
      Init();
    }

    void Update()
    {

      if (delayTime < delay)
      {
        delayTime += UnityEngine.Time.deltaTime;
        return;
      }

      time += UnityEngine.Time.deltaTime * direction;

      Sample(time / duration);

      if ((direction == -1f && time <= 0) || (direction == 1f && time >= duration))
      {

        switch (looping)
        {
          case LoopType.None:
            this.enabled = false;
            break;

          case LoopType.Loop:
            Loop();
            break;

          case LoopType.PingPong:
            Loop(this.direction * -1f);
            break;
        }

      }

    }

    void OnValidate()
    {
      if (duration == 0f)
      {
        duration = 1f;
      }
    }

    #endregion

    #region Tween

    public void Play()
    {
      this.delayTime = 0;
      this.time = 0;
      this.direction = 1f;
      this.enabled = true;
    }

    protected virtual void Init(float initDirection = 1)
    {
      this.direction = initDirection;

      if (this.direction > 0)
      {
        this.time = 0;
      }
      else
      {
        this.time = duration;
      }
    }

    protected virtual void Loop(float initDirection = 1)
    {
      this.direction = initDirection;

      if (this.direction > 0)
      {
        this.time -= duration;
      }
      else
      {
        this.time += duration;
      }
    }

    protected virtual float Ease(float t, float start, float end)
    {
      return Easing.Ease(this.easing, t, start, end - start);
    }

    protected abstract void Sample(float t);

    #endregion

  }

}