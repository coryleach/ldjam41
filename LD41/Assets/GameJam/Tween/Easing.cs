﻿using UnityEngine;
using System.Collections;

namespace GameJam.Tween
{

  public enum EaseType
  {
    Linear,
    SinIn,
    SinOut,
    SinInOut,
    BackIn,
    BackOut,
    BackInOut
  };

  public static class Easing
  {

    public static float Ease(EaseType type, float time, float start, float delta)
    {
      switch (type)
      {
        case EaseType.Linear:
          return Linear(time, start, delta);

        case EaseType.SinIn:
          return SinIn(time, start, delta);

        case EaseType.SinOut:
          return SinOut(time, start, delta);

        case EaseType.SinInOut:
          return SinInOut(time, start, delta);

        case EaseType.BackIn:
          return BackIn(time, start, delta);

        case EaseType.BackOut:
          return BackOut(time, start, delta);

        case EaseType.BackInOut:
          return BackInOut(time, start, delta);

        default:
          return 0;
      }
    }

    public static float Linear(float time, float start, float delta)
    {
      return delta * time + start;
    }

    public static float SinInOut(float time, float start, float delta)
    {
      return (-delta * 0.5f) * (Mathf.Cos(Mathf.PI * time) - 1) + start;
    }

    public static float SinOut(float time, float start, float delta)
    {
      return delta * Mathf.Sin(time * (Mathf.PI * 0.5f)) + start;
    }

    public static float SinIn(float time, float start, float delta)
    {
      return -delta * Mathf.Cos(time * (Mathf.PI * 0.5f)) + delta + start;
    }

    public static float BackIn(float time, float start, float delta)
    {
      float s = 1.70158f;
      return delta * time * time * ((s + 1) * time - s) + start;
    }

    public static float BackOut(float time, float start, float delta)
    {
      float s = 1.70158f;
      float t = time - 1;
      return delta * (t * t * ((s + 1) * t + s) + 1) + start;
    }

    public static float BackInOut(float time, float start, float delta)
    {
      float s = (1.70158f * 1.525f);
      float t = time * 2;
      if (t < 1)
      {
        return delta / 2 * (t * t * ((s + 1) * t - s)) + start;
      }
      else
      {
        t = t - 2;
        return delta / 2 * (t * t * ((s + 1) * t + s) + 2) + start;
      }
    }

  }

}