﻿using UnityEngine;
using System.Collections;

namespace GameJam.Tween
{

  public class TweenRotation : Tweener
  {

    public Vector3 start;
    public Vector3 end;

    protected override void Sample(float t)
    {
      var startQ = Quaternion.Euler(start);
      var endQ = Quaternion.Euler(end);

      var rot = Quaternion.identity;

      rot.x = this.Ease(t, startQ.x, endQ.x);
      rot.y = this.Ease(t, startQ.y, endQ.y);
      rot.z = this.Ease(t, startQ.z, endQ.z);
      rot.w = this.Ease(t, startQ.w, endQ.w);

      this.transform.localRotation = rot;
    }

  }

}