﻿using UnityEngine;
using System.Collections;

namespace GameJam.Tween
{

  public class TweenPosition : Tweener
  {

    public Vector3 start;
    public Vector3 end;

    protected override void Sample(float t)
    {
      Vector3 pt = Vector3.zero;

      pt.x = this.Ease(t, start.x, end.x);
      pt.y = this.Ease(t, start.y, end.y);
      pt.z = this.Ease(t, start.z, end.z);

      this.transform.localPosition = pt;
    }

  }

}